import timeit
from typing import List, Tuple, Optional
from a2019 import helper

from itertools import permutations

data = None


def manhattan(pos: Tuple[int, int]):
    """returns the Manhattan distance from origin"""
    return abs(pos[0]) + abs(pos[1])


def wire_gen(path: List[str]):
    """Generator outputting the (x, y) a wire follows
    
    Input looks like ["L1003", "U603", ... ]
    """
    x, y = 0, 0 
    for instruction in path:
        dir = instruction[0]
        count = int(instruction[1:])
        
        if dir == 'U':
            dx, dy = 0, 1
        elif dir == 'L':
            dx, dy = -1, 0
        elif dir == 'D':
            dx, dy = 0, -1
        elif dir == 'R':
            dx, dy = 1, 0
        else:
            raise ValueError
        
        for i in range(count):
            x += dx
            y += dy
            yield (x, y)
        

class Wire:
    def __init__(self, path):
        self.path = path
        self.gen = wire_gen(self.path)
        self.history = {}
        self.last_pos = (0, 0)
        self.steps = 0
    
    def step(self):
        if not self.gen:
            return self.last_pos
        
        self.steps += 1
        
        # advance if we still have wire left
        try:
            pos = next(self.gen)
        except StopIteration:
            self.gen = None
            return self.last_pos
        
        # record the lowest step count for each pos 
        if pos not in self.history:
            self.history[pos] = self.steps

        self.last_pos = pos
        return pos
        

def phase1(data: List[List[str]]):
    wires = [Wire(x) for x in data]
    
    while any(x.gen for x in wires):
        latest = []
        for wire in wires:
            latest.append(wire.step())
        
        # see if any of the new positions are in all wires
        for pos in latest:
            if all((pos in x.history) for x in wires):
                print(pos, manhattan(pos), sum(x.history[pos] for x in wires))

    
def main():
    global data
    data = [ x.split(",") for x in helper.data_list("day3.txt") ]

    phase1(data)    

    # print("timed")
    # print(timeit.timeit('phase1(data, test_pw1)', number=1000, globals=globals()))
    # print(timeit.timeit('phase1(data, test_pw2)', number=1000, globals=globals()))


if __name__ == "__main__":
    main()
