import timeit
from typing import List, Tuple, Optional, Iterable, Generator
from a2019 import helper

from itertools import permutations

from multiprocessing import Process, Queue
import queue
import time

data = None


class ParameterView:
    def __init__(self, data: List[int], ip: int, size: int, parameter_mode: int):
        """Awesome class to automatically handle parameter modes"""
        self.orig_data = data
        self.data = data[ip:ip + size]
        self.ip = ip
        self.size = size
        self.parameter_mode = parameter_mode

        # parse the parameter mode, modifying our view
        i = 1
        while i < size:
            parameter_mode, mode = divmod(parameter_mode, 10)
            if mode == 0:
                # position mode, deref
                self.data[i] = data[data[ip + i]]
            elif mode == 1:
                # immediate mode, leave as is
                pass
            else:
                raise NotImplementedError

            i += 1

    def __iter__(self):
        return (x for x in self.data)

    def __getitem__(self, key):
        return self.data[key]

    def __setitem__(self, key, value):
        # writes are always deref'd 
        addr = self.orig_data[self.ip + key]
        # if addr == 0:
        #     print("Write to zero??")
        self.orig_data[self.orig_data[self.ip + key]] = value


def parse_op(opcode: int) -> Tuple[int, int]:
    """returns (parameter_mode, op)"""
    return divmod(opcode, 100)


class IntcodeMachine(Process):
    def __init__(self, data: list, *args, input: Optional[Queue] = None, output: Optional[Queue] = None, **kwargs):
        Process.__init__(self, *args, **kwargs)

        if not input:
            input = Queue()
        if not output:
            output = Queue()

        self.data = data[:]
        self.input = input
        self.output = output

    def run(self):
        self.intcode()

    def intcode(self):
        """run our intcode program"""
        data = self.data

        ip = 0
        while True:
            parameter_mode, op = parse_op(data[ip])

            if op == 1:
                # add
                p = ParameterView(data, ip, 4, parameter_mode)
                p[3] = p[1] + p[2]
                ip += p.size
            elif op == 2:
                # mul
                p = ParameterView(data, ip, 4, parameter_mode)
                p[3] = p[1] * p[2]
                ip += p.size
            elif op == 3:
                # inp
                assert parameter_mode == 0
                p = ParameterView(data, ip, 2, parameter_mode)
                p[1] = self.input.get()
                ip += 2
            elif op == 4:
                # out
                p = ParameterView(data, ip, 2, parameter_mode)
                self.output.put(p[1])
                ip += p.size
            elif op == 5:
                # jt
                p = ParameterView(data, ip, 3, parameter_mode)
                if p[1] != 0:
                    ip = p[2]
                else:
                    ip += p.size
            elif op == 6:
                # jf
                p = ParameterView(data, ip, 3, parameter_mode)
                if p[1] == 0:
                    ip = p[2]
                else:
                    ip += p.size
            elif op == 7:
                # lt
                p = ParameterView(data, ip, 4, parameter_mode)
                if p[1] < p[2]:
                    p[3] = 1
                else:
                    p[3] = 0
                ip += p.size
            elif op == 8:
                # eq
                p = ParameterView(data, ip, 4, parameter_mode)
                if p[1] == p[2]:
                    p[3] = 1
                else:
                    p[3] = 0
                ip += p.size
            elif op == 99:
                # rip
                break
            else:
                raise ValueError("Bad opcode")

        # clean up
        self.output.close()


def run_static(data, input: Iterable[int]):
    input = list(input)
    retval = []
    
    machine = IntcodeMachine(data)
    machine.start()
    while machine.is_alive():
        # feed the machine
        if input and machine.is_alive() and machine.input.empty():
            machine.input.put(input.pop(0))

        # milk the machine:
        try:
            retval.append(machine.output.get_nowait())
        except queue.Empty:
            pass

        time.sleep(0.01)

    # drain the output
    while not machine.output.empty():
        try:
            retval.append(machine.output.get_nowait())
        except queue.Empty:
            pass

    machine.join()
    return retval


def phase1(data):
    results = []
    for orders in permutations(range(5)):
        print(orders)
        output = 0
        for setting in orders:
            new_output = run_static(data, (setting, output))
            print(setting, output, "->", new_output)
            output = new_output[0]
        results.append((output, orders))

    print(sorted(results))


def phase2(data):
    results = []
    for orders in permutations(range(5,10)):
        print(orders)
        output = 0

        # chain some machines together
        machines = []   # type: List[IntcodeMachine]
        cluster_input = Queue()
        next_input = cluster_input
        next_output = Queue()
        
        for setting in orders:
            next_input.put(setting)
            machine = IntcodeMachine(data, input=next_input, output=next_output)
            machine.start()
            machines.append(machine)

            next_input = next_output
            next_output = Queue()
            
        cluster_output = machine.output
        
        # prime the loop
        cluster_input.put(0)
        
        # feedback time
        while machines[-1].is_alive():
            # milk the machine:
            try:
                out = machine.output.get(timeout=0.001)
            except queue.Empty:
                pass
            else:
                # print(f"/\\/\\/\\ Feedback: {out} /\\/\\/\\")
                cluster_input.put(out)
                results.append(out)
        
        # flush:
        try:
            out = machine.output.get_nowait()
        except queue.Empty:
            pass
        else:
            print(f"/\\/\\/\\ Feedback: {out} /\\/\\/\\")
            cluster_input.put(out)
            results.append(out)

        print("machines done: " + str(out))
    
    print(max(results))


def main():
    global data
    data = [int(x) for x in helper.data_raw("day7.txt").split(",")]
    
    # phase1(data)
    phase2(data)
    
    
    
    # print("timed")
    # print(timeit.timeit('phase1(data, test_pw1)', number=1000, globals=globals()))
    # print(timeit.timeit('phase1(data, test_pw2)', number=1000, globals=globals()))


if __name__ == "__main__":
    main()
