import timeit
from typing import List, Tuple, Optional
from a2019 import helper

from math import floor

data = None


def phase1(data):
    return sum(floor(x / 3) - 2 for x in data)


def p2_mass(mass: int):
    total_fuel = 0
    step_fuel = mass
    while True:
        step_fuel = floor(step_fuel / 3) - 2
        if step_fuel <= 0:
            break
        total_fuel += step_fuel
    return total_fuel


def phase2(data):
   return sum(p2_mass(x) for x in data)
        

def main():
    global data
    data = [int(x) for x in helper.data_list("day1.txt")]

    print(phase1(data))
    print(phase2(data))

    print("timed")
    # print(timeit.timeit('phase1(data, test_pw1)', number=1000, globals=globals()))
    # print(timeit.timeit('phase1(data, test_pw2)', number=1000, globals=globals()))


if __name__ == "__main__":
    main()
