import timeit
from typing import List, Tuple, Optional
from a2019 import helper

from itertools import permutations
import re

data = None


# TODO: rewrite using regex

def test_pw(pw: int, min, max):
    """Return False if not a valid pw"""
    s = str(pw)
    
    doubles = False
    last_char = '-1'
    for char in s:
        # rule 3 part 1
        if char == last_char:
            doubles = True
            
        # rule 4
        if int(last_char) > int(char):
            return False
        
        last_char = char

    # rule 3 part 2
    if not doubles:
        return False

    # rule 2
    if not min <= pw <= max:
        return False

    # rule 1
    if len(s) != 6:
        return False
    
    return True


def test_pw_2(pw: int, min, max):
    """Return False if not a valid pw"""
    s = str(pw)

    doubles = False
    in_run = False
    rule_3 = False
    last_char = '-1'
    for char in s:
        # rule 3 part 1
        if char == last_char and not rule_3:
            if in_run:
                # phase 2 rule 3 part 1
                doubles = False
            else:
                doubles = True
                in_run = True
        else:
            if in_run:
                if doubles:
                    # phase 2 rule 3 part 2
                    # if a run ended without hitting triples, we pass rule 3
                    rule_3 = True

                in_run = False
                

        # rule 4
        if int(last_char) > int(char):
            return False

        last_char = char

    # rule 3 part 2
    if not doubles:
        return False

    # rule 2
    if not min <= pw <= max:
        return False

    # rule 1
    if len(s) != 6:
        return False

    return True


def test_pw_pre(pw: int, min, max):
    """Return False if not a valid pw"""
    s = str(pw)

    doubles = False
    last_char = '-1'
    for char in s:
        # rule 4
        if int(last_char) > int(char):
            return False

        last_char = char
    
    # rule 2
    if not min <= pw <= max:
        return False

    # rule 1
    if len(s) != 6:
        return False
    
    # rule 3
    double_re = re.compile(r"(11|22|33|44|55|66|77|88|99|00)")
    if not double_re.search(s):
        return False

    return True

def phase(data: Tuple[int, int], test_func):
    min, max = data
    
    valid_count = 0
    
    for pw in range(min, max + 1):
        if test_func(pw, min, max):
            valid_count += 1
    
    return valid_count


def wtf(data: Tuple[int, int]):
    min, max = data

    valid_count = 0

    for pw in range(min, max + 1):
        if test_pw(pw, min, max):
            if not test_pw_pre(pw, min, max):
                print(pw)
                assert False

    return valid_count


def main():
    global data
    data = (353096, 843212)

    # wtf(data)

    print(phase(data, test_pw))
    print(phase(data, test_pw_pre))
    print(phase(data, test_pw_2))

    print("timed")
    print(timeit.timeit('phase(data, test_pw)', number=3, globals=globals()))
    print(timeit.timeit('phase(data, test_pw_pre)', number=3, globals=globals()))
    print(timeit.timeit('phase(data, test_pw_2)', number=3, globals=globals()))


if __name__ == "__main__":
    main()
