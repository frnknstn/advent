import timeit
from typing import List, Tuple, Optional
from a2019 import helper

from itertools import permutations

from multiprocessing import Process, Queue
import queue
import time

data = None


class ParameterView:
    def __init__(self, data: List[int], ip: int, size: int, parameter_mode: int):
        """Awesome class to automatically handle parameter modes"""
        self.orig_data = data
        self.data = data[ip:ip+size]
        self.ip = ip
        self.size = size
        self.parameter_mode = parameter_mode
        
        # parse the parameter mode, modifying our view
        i = 1
        while i < size:
            parameter_mode, mode = divmod(parameter_mode, 10)
            if mode == 0:
                # position mode, deref
                self.data[i] = data[data[ip+i]]
            elif mode == 1:
                # immediate mode, leave as is
                pass
            else:
                raise NotImplementedError
            
            i += 1
    
    def __iter__(self):
        return (x for x in self.data)
    
    def __getitem__(self, key):
        return self.data[key]
    
    def __setitem__(self, key, value):
        # writes are always deref'd 
        addr = self.orig_data[self.ip + key]
        self.orig_data[self.orig_data[self.ip + key]] = value


def parse_op(opcode: int) -> Tuple[int, int]:
    """returns (parameter_mode, op)"""
    return divmod(opcode, 100)


class IntcodeMachine(Process):
    def __init__(self, data: list, *args, input: Optional[Queue] = None, output: Optional[Queue] = None, **kwargs):
        Process.__init__(self, *args, **kwargs)
        
        if not input:
            input = Queue()
        if not output:
            output = Queue()
            
        self.data = data[:]
        self.input = input
        self.output = output
    
    def run(self):
        self.intcode()
    
    def intcode(self):
        """run our intcode program"""
        data = self.data

        ip = 0
        while True:
            parameter_mode, op = parse_op(data[ip])
    
            if op == 1:
                # add
                p = ParameterView(data, ip, 4, parameter_mode)
                p[3] = p[1] + p[2]
                ip += p.size
            elif op == 2:
                # mul
                p = ParameterView(data, ip, 4, parameter_mode)
                p[3] = p[1] * p[2]
                ip += p.size
            elif op == 3:
                # inp
                assert parameter_mode == 0
                p = ParameterView(data, ip, 2, parameter_mode)
                p[1] = self.input.get()
                ip += 2
            elif op == 4:
                # out
                p = ParameterView(data, ip, 2, parameter_mode)
                self.output.put(p[1])
                ip += p.size
            elif op == 5:
                # jt
                p = ParameterView(data, ip, 3, parameter_mode)
                if p[1] != 0:
                    ip = p[2]
                else:
                    ip += p.size
            elif op == 6:
                # jf
                p = ParameterView(data, ip, 3, parameter_mode)
                if p[1] == 0:
                    ip = p[2]
                else:
                    ip += p.size
            elif op == 7:
                # lt
                p = ParameterView(data, ip, 4, parameter_mode)
                if p[1] < p[2]:
                    p[3] = 1
                else:
                    p[3] = 0
                ip += p.size
            elif op == 8:
                # eq
                p = ParameterView(data, ip, 4, parameter_mode)
                if p[1] == p[2]:
                    p[3] = 1
                else:
                    p[3] = 0
                ip += p.size
            elif op == 99:
                # rip
                break
            else:
                raise ValueError("Bad opcode")
        
        # clean up
        self.output.close()


def phase1(data, input: List[int]):
    retval = []
    machine = IntcodeMachine(data)
    machine.start()
    while machine.is_alive():
        # feed the machine
        if input and machine.is_alive() and machine.input.empty():
            machine.input.put(input.pop(0))
        
        # milk the machine:
        try:
            retval.append(machine.output.get_nowait())
        except queue.Empty:
            pass
        
        time.sleep(0.01)
    
    # drain the output
    while not machine.output.empty():
        try:
            retval.append(machine.output.get_nowait())
        except queue.Empty:
            pass
    
    machine.join()
    return retval


def main():
    global data
    data = [int(x) for x in helper.data_raw("day5.txt").split(",")]
    
    print(phase1(data[:], [1]))     # phase 1
    print(phase1(data[:], [5]))     # phase 2

    # tests
    
    # outputs whatever it gets as input, then halts.
    test = [3,0,4,0,99]
    print(phase1(test, [-11]))    
    
    print("here are several programs that take one input, compare it to the value 8, and then produce one output:")
    print("Using position mode, consider whether the input is equal to 8; output 1 (if it is) or 0 (if it is not).")
    test = [3,9,8,9,10,9,4,9,99,-1,8]
    print(phase1(test, [8]))
    print(phase1(test, [0]))
    print("Using position mode, consider whether the input is less than 8; output 1 (if it is) or 0 (if it is not).")
    test = [3,9,7,9,10,9,4,9,99,-1,8]
    print(phase1(test, [8]))
    print(phase1(test, [0]))
    #  Using immediate mode, consider whether the input is equal to 8; output 1 (if it is) or 0 (if it is not).
    test = [3,3,1108,-1,8,3,4,3,99]
    print(phase1(test, [8]))
    print(phase1(test, [0]))
    #  Using immediate mode, consider whether the input is less than 8; output 1 (if it is) or 0 (if it is not).
    test = [3,3,1107,-1,8,3,4,3,99]
    print(phase1(test, [8]))
    print(phase1(test, [0]))

    # Here are some jump tests that take an input, then output 0 if the input was zero or 1 if the input was non-zero:
    test1 = [3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9]  # (using position mode)
    test2 = [3,3,1105,-1,9,1101,0,0,12,4,12,99,1] # (using immediate mode)

    print(phase1(test1, [0]))
    print(phase1(test1, [1]))
    print(phase1(test2, [0]))
    print(phase1(test2, [1]))

    t2 = [3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,
          0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99]
    print(phase1(t2[:], [0]))
    print(phase1(t2[:], [8]))
    print(phase1(t2[:], [12]))


    # print("timed")
    # print(timeit.timeit('phase1(data, test_pw1)', number=1000, globals=globals()))
    # print(timeit.timeit('phase1(data, test_pw2)', number=1000, globals=globals()))


if __name__ == "__main__":
    main()
