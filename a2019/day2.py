import timeit
from typing import List, Tuple, Optional
from a2019 import helper

from itertools import permutations

data = [1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,1,10,19,1,19,6,23,2,23,13,27,1,27,5,31,2,31,10,35,1,9,35,39,1,39,9,43,2,9,43,47,1,5,47,51,2,13,51,55,1,55,9,59,2,6,59,63,1,63,5,67,1,10,67,71,1,71,10,75,2,75,13,79,2,79,13,83,1,5,83,87,1,87,6,91,2,91,13,95,1,5,95,99,1,99,2,103,1,103,6,0,99,2,14,0,0]



def phase1(data, noun, verb):
    data[1] = noun
    data[2] = verb
    i = 0
    while True:
        op = data[i]
        if op == 1:
            data[data[i+3]] = data[data[i+1]] + data[data[i+2]]
        elif op == 2:
            data[data[i + 3]] = data[data[i+1]] * data[data[i+2]]
        elif op == 99:
            break
        
        i += 4
    return data[0]


def phase2(data, target):
    for noun, verb in permutations(range(100), 2):
        result = phase1(data[:], noun, verb)
        if result == target:
            return 100 * noun + verb
    

def main():
    global data
    
    print(phase1(data[:], 12, 2))
    print(phase2(data, 19690720))

    # print("timed")
    # print(timeit.timeit('phase1(data, test_pw1)', number=1000, globals=globals()))
    # print(timeit.timeit('phase1(data, test_pw2)', number=1000, globals=globals()))


if __name__ == "__main__":
    main()
