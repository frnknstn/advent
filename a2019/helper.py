from typing import Tuple
import os

module = "a2019"


def data_raw(filename):
    with open(os.path.join(module, filename), "rt") as fp:
        return fp.read()


def data_list(filename):
    with open(os.path.join(module, filename), "rt") as fp:
        data = fp.read()
        return data.strip().split("\n")


def manhattan(pos: Tuple[int, int]):
    """returns the Manhattan distance from origin"""
    return abs(pos[0]) + abs(pos[1])
