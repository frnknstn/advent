import timeit
from typing import List, Tuple, Optional, Dict, Iterable
from a2019 import helper

import sys
from itertools import permutations

data = None

nodes = {}  # type: Dict[str, Node]


class Node:
    def __init__(self, parent_node: str, node_name: str):
        self.name = node_name
        self.parent = parent_node
        self.checksum = 0
        self.children = []  # type: List[str]
    
    def set_checksum(self, i):
        """fill this tree with sums!"""
        self.checksum = i
        for node_name in self.children:
            nodes[node_name].set_checksum(i + 1)


def phase1(data: Iterable[Tuple[str, str]]):
    print("Building nodes")
    root = Node("", "COM")
    nodes["COM"] = root
    for parent_node, node_name in data:
        nodes[node_name] = Node(parent_node, node_name)
        
    print("Linking children")
    for node_name, node in nodes.items():
        if node.parent:
            nodes[node.parent].children.append(node_name)
    
    print("Calculating node checksums")
    root.set_checksum(0)
    
    print("Total checksum:")
    check_total = sum(x.checksum for x in nodes.values())
    print(check_total)


def list_parents(node_name: Node) -> List[str]:
    """List all parent node names from given node to the root, in that order"""
    retval = []
    while True:
        node = nodes[node_name]
        parent = node.parent
        
        if parent:
            retval.append(parent)
            node_name = parent
        else:
            # found the root
            break
    
    return retval


def common_orbit(a: str, b: str):
    """work out the name of the common parent node between nodes named a and b"""
    a_parents = list_parents(a)
    b_parents = list_parents(b)
    print(f"Parents of {a}: {a_parents}")
    print(f"Parents of {b}: {b_parents}")
    
    b_set = set(b_parents)
    for node_name in a_parents:
        if node_name in b_set:
            common_node = nodes[node_name]
            print(f"found the common parent {node_name}: orbit {common_node.checksum}")
            break
    
    a_transfer = nodes[a].checksum - common_node.checksum - 1
    b_transfer = nodes[b].checksum - common_node.checksum - 1
    print(a_transfer, b_transfer, a_transfer + b_transfer)
    
    
            
    
    
    

def phase2():
    # calculate co
    common_orbit("YOU", "SAN")


def main():
    global data
    data = [x.split(")") for x in helper.data_list("day6.txt")]
    sys.setrecursionlimit(sys.getrecursionlimit() + len(data))

    phase1(data)
    phase2()

    # print("timed")
    # print(timeit.timeit('phase1(data, test_pw1)', number=1000, globals=globals()))
    # print(timeit.timeit('phase1(data, test_pw2)', number=1000, globals=globals()))


if __name__ == "__main__":
    main()
