from typing import List, Tuple, Optional, Iterable

from multiprocessing import Process, Queue, Event
from queue import Empty as QueueEmpty 
import time


class ParameterView:
    def __init__(self, data: List[int], max_size: int = 4):
        self.orig_data = data
        
        self.data = [0] * max_size 
        self.data_addr = data[:]

        self.relative_base = 0    
        
    def view(self, ip: int, size: int, parameter_mode: int):
        """Awesome class to automatically handle parameter modes"""
        data = self.orig_data
        local_data = self.data
        local_data_addr = self.data_addr

        # parse the parameter mode, modifying our view
        i = 1
        while i < size:
            parameter_mode, mode = divmod(parameter_mode, 10)
            if mode == 0:
                # position mode, deref
                local_data_addr[i] = data[ip + i]
                local_data[i] = data[data[ip + i]]
            elif mode == 1:
                # immediate mode, copy as is
                local_data_addr[i] = ip + i
                local_data[i] = data[ip + i]
            elif mode == 2:
                # relative_base
                local_data_addr[i] = self.relative_base + data[ip + i]
                local_data[i] = data[self.relative_base + data[ip + i]]
            else:
                raise NotImplementedError

            i += 1

    def __iter__(self):
        return (x for x in self.data)

    def __getitem__(self, key):
        return self.data[key]

    def __setitem__(self, key, value):
        self.orig_data[self.data_addr[key]] = value


class IntcodeMachine(Process):
    def __init__(self, data: list, *args, input: Optional[Queue] = None, output: Optional[Queue] = None, size=None,
                 **kwargs):
        Process.__init__(self, *args, **kwargs)

        if not input:
            input = Queue()
        if not output:
            output = Queue()
        if not size:
            size = len(data) * 16

        self.data = data[:] + [0] * (size - len(data))  # add extra work memory
        self.input = input
        self.output = output
        self.input_blocked = Event()
        # this is not actually foolproof

    def run(self):
        self.intcode()

    def intcode(self):
        """run our intcode program"""
        data = self.data
        p = ParameterView(data, 4)

        ip = 0
        while True:
            parameter_mode, op = divmod(data[ip], 100)

            if op == 1:
                # add
                size = 4
                p.view(ip, size, parameter_mode)
                p[3] = p[1] + p[2]
                ip += size
            elif op == 2:
                # mul
                size = 4
                p.view(ip, size, parameter_mode)
                p[3] = p[1] * p[2]
                ip += size
            elif op == 3:
                # inp
                assert parameter_mode in (0, 2)
                size = 2
                p.view(ip, size, parameter_mode)
                self.input_blocked.set()
                val = self.input.get()
                self.input_blocked.clear()
                p[1] = val
                ip += size
            elif op == 4:
                # out
                size = 2
                p.view(ip, size, parameter_mode)
                self.output.put(p[1])
                ip += size
            elif op == 5:
                # jt
                size = 3
                p.view(ip, size, parameter_mode)
                if p[1] != 0:
                    ip = p[2]
                else:
                    ip += size
            elif op == 6:
                # jf
                size = 3
                p.view(ip, size, parameter_mode)
                if p[1] == 0:
                    ip = p[2]
                else:
                    ip += size
            elif op == 7:
                # lt
                size = 4
                p.view(ip, size, parameter_mode)
                if p[1] < p[2]:
                    p[3] = 1
                else:
                    p[3] = 0
                ip += size
            elif op == 8:
                # eq
                size = 4
                p.view(ip, size, parameter_mode)
                if p[1] == p[2]:
                    p[3] = 1
                else:
                    p[3] = 0
                ip += size
            elif op == 9:
                # rof
                size = 2
                p.view(ip, size, parameter_mode)
                p.relative_base += p[1]
                ip += size
            elif op == 99:
                # rip
                break
            else:
                raise ValueError(f"Bad opcode {op} at {ip}")

        # clean up
        self.output.close()


def run_static(data, input: Iterable[int], machine_type=IntcodeMachine):
    input = list(input)
    retval = []

    machine = machine_type(data)
    machine.start()
    while machine.is_alive():
        # feed the machine
        if input and machine.is_alive() and machine.input.empty():
            machine.input.put(input.pop(0))

        # milk the machine:
        try:
            retval.append(machine.output.get_nowait())
        except QueueEmpty:
            pass

        time.sleep(0.001)

    # drain the output
    while not machine.output.empty():
        try:
            retval.append(machine.output.get_nowait())
        except QueueEmpty:
            pass

    machine.join()
    return retval
