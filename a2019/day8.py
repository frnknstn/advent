import timeit
from typing import List, Tuple, Optional, Iterable, Generator
from a2019 import helper

from itertools import permutations

from multiprocessing import Process, Queue
import queue
import time

data = None


def phase1(data):
    results = []
    layer_id = 0
    offset = 0
    while offset < len(data):
        counts = [0,0,0]
        for y in range(6):
            row = data[offset:offset + 25]
            print(row)
            offset += 25
            for i in range(3):
                counts[i] += row.count(str(i))

        # end of layer
        print(counts)
        results.append((counts, layer_id))
        layer_id += 1
    
    print(min(results))


def phase2(data):
    layers = []
    layer_id = 0
    offset = 0
    while offset < len(data):
        counts = [0, 0, 0]
        layer = []
        for y in range(6):
            row = data[offset:offset + 25]
            layer.append(row)
            offset += 25
            for i in range(3):
                counts[i] += row.count(str(i))

        # end of layer
        layers.append(layer)
        layer_id += 1

    print(len(layers))
    
    # join layers
    image = ['2'*25 for x in range(6)]
    for layer in reversed(layers):
        for row, (base, over) in enumerate(zip(image, layer)):
            image[row] = "".join(over_c if over_c != "2" else base_c 
                                 for base_c, over_c in zip(base, over))
    
    # show
    for row in image:
        print(row.replace('1', "▓").replace("0", "░"))


def main():
    global data
    data = helper.data_raw("day8.txt")

    # phase1(data)
    phase2(data)

    # print("timed")
    # print(timeit.timeit('phase1(data, test_pw1)', number=1000, globals=globals()))
    # print(timeit.timeit('phase1(data, test_pw2)', number=1000, globals=globals()))


if __name__ == "__main__":
    main()
