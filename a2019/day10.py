import timeit
from typing import List, Tuple, Optional, Iterable, Generator, Set, Dict, Union
from a2019 import helper

from itertools import permutations

from multiprocessing import Process, Queue
import queue
import time
import math

data = None


def draw_map(map):
    for row in map:
        print(row)


def draw_results(data_map: Iterable[Iterable[int]]):
    for row in data_map:
        print("".join(str(x) if x else '.' for x in row))


def set_to_map(asteroids: Set[Tuple[int, int]]) -> List[str]:
    # work out size of map
    max_x, max_y = 0, 0
    for a in asteroids:
        max_x, max_y = max(max_x, a[0]), max(max_y, a[1])
    
    # make map
    map = [ list('.') * (max_x + 1) for i in range(max_y + 1) ]
    for x, y in asteroids:
        map[y][x] = '#'
    
    return [''.join(row) for row in map] 


def map_to_set(map: List[str]) -> Set[Tuple[int, int]]:
    retval = set()
    for y, row in enumerate(map):
        for x, c in enumerate(row):
            if c == '#':
                retval.add((x, y))
    return retval



# def set_to_map(asteroids: Dict[Tuple[int, int], int]]) -> List[str]:


def simplify_delta(x, y) -> Tuple[int, int]:
    factor = math.gcd(x, y)
    while factor != 1:
        x, y = int(x/factor), int(y/factor)
        factor = math.gcd(x, y)
    return (x, y)


def count_visible(asteroids: Set[Tuple[int, int]], home: Tuple[int, int], size: Tuple[int, int]):
    """Count the number of asteroids visible from the asteroid at the given home location"""
    assert home in asteroids
    
    # working copy
    asteroids = set(asteroids)
    asteroids.remove(home)
    all_asteroids = tuple(asteroids)
    
    # cull invisible asteroids
    for pos in all_asteroids:
        if pos not in asteroids:
            # already excluded
            continue
        
        # exclude object further than this one that are on the same slope
        dx, dy = simplify_delta(pos[0] - home[0], pos[1] - home[1])
        x, y = pos[0], pos[1]
        while True:
            x, y = x + dx, y + dy
            if not (0 <= x < size[0] and 0 <= y < size[1]):
                # out of bounds
                break
            
            if (x, y) in asteroids:
                asteroids.remove((x, y))
    
                
    return len(asteroids)


def phase1(data):
    draw_map(data)
    asteroids = map_to_set(data)
    size = (len(data[0]), len(data))
    results = [ [0] * (size[0]) for i in range(size[1]) ]
    best = (0, (0, 0))

    # process all the asteroids
    for home in asteroids:
        c = count_visible(asteroids, home, size)
        results[home[1]][home[0]] = c
        if c > best[0]:
            best = (c, (home))
    
    # show our results map
    draw_results(results)
    return best


def phase2(data, home):
    draw_map(data)
    asteroids = map_to_set(data)
    size = (len(data[0]), len(data))
    
    sortable = []

    # process all the asteroids
    asteroids.remove(home)
    for x, y in asteroids:
        # work out the slopes etc for ordering
        raw_delta = x - home[0], y - home[1]
        range = helper.manhattan(raw_delta)
        dx, dy = (x - home[0], y - home[1])

        if dx >= 0:
            hemi = 0
        else:
            hemi = 1
        
        if dx == 0:
            if dy > 0:
                slope = float("+inf")
            else:
                slope = float("-inf")
        else:
            dx, dy = simplify_delta(dx, dy)
            slope = dy / dx

        sortable.append((hemi, slope, range, (x, y)))
    
    sortable.sort()
    
    # shoot the frontmost first
    total_order = []
    now = sortable
    later = []
    last_shot = None
    while True:
        if len(now) == 0:
            if not later:
                # done all asteroids
                break
            else:
                # start the next rotation
                now = later
                later = []
                last_shot = None
                continue
        
        curr = now.pop(0)
        hemi, slope, range, (x, y) = curr
        if last_shot and last_shot[0:2] == (hemi, slope):
            # behind the current target, leave this one for later
            later.append(curr)
            print("delay " + str(curr))
        else:
            # shoot it
            total_order.append(curr)
            last_shot = curr
    
    return total_order
            

def main():
    global data
    data = helper.data_list("day10.txt")

    test_data = """\
.#....#####...#..
##...##.#####..##
##...#...#.#####.
..#.....#...###..
..#.#.....#....##
""".strip().split("\n")
    # data = test_data

    count, home = phase1(data)
    print(count, home)
    order = phase2(data, home)
    print(order[199])
    
#     test_data = """\
# .#..#
# .....
# #####
# ....#
# ...##
# """.strip().split("\n")
#     
#     phase1(test_data)

    # print("timed")
    # print(timeit.timeit('phase1(data, test_pw1)', number=1000, globals=globals()))
    # print(timeit.timeit('phase1(data, test_pw2)', number=1000, globals=globals()))


if __name__ == "__main__":
    main()
