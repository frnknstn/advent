import timeit
from typing import List, Tuple, Optional, Iterable, Generator
from a2019 import helper

from itertools import permutations

from multiprocessing import Process, Queue
from queue import Empty as QueueEmpty
import time

from a2019.intcode import run_static, IntcodeMachine

data = None


class Robot:
    def __init__(self, roof_size=(160, 160), start_color:int=0):
        self.x = roof_size[0] // 2
        self.y = roof_size[1] // 2
        self.dir = 0    # 0: up, 1: right, 2: down, 3: left
        
        self.state = 0
        
        self.roof = [[0] * roof_size[0] for x in range(roof_size[1])]
        self.paint_history = set()

        self.paint(start_color)

    def curr_color(self):
        return self.roof[self.y][self.x]
    
    def process(self, val):
        if self.state == 0:
            # waiting for paint order
            self.paint(val)
            self.state = 1
        else:
            # waiting for move order
            self.move(val)
            self.state = 0
    
    def paint(self, color):
        self.roof[self.y][self.x] = color
        pos = (self.x, self.y)
        if pos not in self.paint_history:
            self.paint_history.add((self.x, self.y))
            print(f"new paint #{len(self.paint_history)} {pos}")
    
    def move(self, cw: int):
        # turn
        if cw == 1:
            # clockwise
            self.dir += 1
            while self.dir >= 4:
                self.dir -= 4
        else:
            # anticlockwise
            self.dir -= 1
            while self.dir < 0:
                self.dir += 4
        
        # move
        if self.dir == 0:
            self.y -= 1
        elif self.dir == 1:
            self.x += 1
        elif self.dir == 2:
            self.y += 1
        elif self.dir == 3:
            self.x -= 1
            
        if self.x < 0 or self.y < 0 or self.x >= len(self.roof[0]) or self.y >= len(self.roof):
            raise IndexError(str((self.x, self.y)))
    
    def draw_roof(self):
        palette = ("░", "▓")
        for row in self.roof:
            print("".join(palette[x] for x in row))
            

def milk(machine, sink):
    while True:
        try:
            output = machine.output.get_nowait()
        except QueueEmpty:
            return
        else:
            sink(output)

def phase1(data, start_color:int = 0):
    robot = Robot(start_color=start_color)
    robot.draw_roof()
    print(min(x[0] for x in robot.paint_history))
    print(max(x[0] for x in robot.paint_history))
    print(min(x[1] for x in robot.paint_history))
    print(max(x[1] for x in robot.paint_history))

    
    machine = IntcodeMachine(data)
    machine.start()
    while machine.is_alive():
        # milk the machine:
        milk(machine, robot.process)

        # feed the machine
        if machine.is_alive() and machine.input_blocked.is_set() and machine.input.empty():
            milk(machine, robot.process)
            machine.input_blocked.clear()
            machine.input.put(robot.curr_color())

        time.sleep(0.001)

    # drain the output
    milk(machine, robot.process)
    machine.join()
    
    robot.draw_roof()
    print(min(x[0] for x in robot.paint_history))
    print(max(x[0] for x in robot.paint_history))
    print(min(x[1] for x in robot.paint_history))
    print(max(x[1] for x in robot.paint_history))

    return robot.paint_history
    
    
def phase2(data, machine):
    return run_static(data, [2])


def main():
    global data
    data = [int(x) for x in helper.data_raw("day11.txt").split(",")]

    # print(phase1(data))
    print(phase1(data, 1))

    # print("timed ==")
    # n = 4
    # print(timeit.timeit('phase1(data)', number=n, globals=globals()))
    # print(timeit.timeit('phase2(data, IntcodeMachine)', number=n, globals=globals()))


if __name__ == "__main__":
    main()
