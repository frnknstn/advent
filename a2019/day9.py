import timeit
from typing import List, Tuple, Optional, Iterable, Generator
from a2019 import helper

from itertools import permutations

from multiprocessing import Process, Queue
import queue
import time

from a2019.intcode import run_static, IntcodeMachine

data = None



def phase1(data, machine):
    # tests
    # print("takes no input and produces a copy of itself as output.")
    # print(run_static([109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99], []))
    # 
    # print("should output a 16-digit number.")
    # print(run_static([1102,34915192,34915192,7,4,7,99,0 ], []))
    # 
    # print(run_static([104,1125899906842624,99], []))
    
    output = run_static(data, [1])
    return output


def phase2(data, machine):
    return run_static(data, [2])


def main():
    global data
    data = [int(x) for x in helper.data_raw("day9.txt").split(",")]

    print(phase1(data, IntcodeMachine))
    print(phase2(data, IntcodeMachine))

    # print("timed ==")
    # n=4
    # print(timeit.timeit('phase1(data, IntcodeMachine)', number=n, globals=globals()))
    # print(timeit.timeit('phase2(data, IntcodeMachine)', number=n, globals=globals()))


if __name__ == "__main__":
    main()
