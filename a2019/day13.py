import timeit
from typing import List, Tuple, Optional, Iterable, Generator
from a2019 import helper

from itertools import permutations

from multiprocessing import Process, Queue
from queue import Empty as QueueEmpty
import time

from a2019.intcode import run_static, IntcodeMachine

data = None


def milk(machine, sink):
    while True:
        try:
            output = machine.output.get_nowait()
        except QueueEmpty:
            return
        else:
            sink(output)


def phase1(data, start_color: int = 0):
    robot = Robot(start_color=start_color)
    robot.draw_roof()
    print(min(x[0] for x in robot.paint_history))
    print(max(x[0] for x in robot.paint_history))
    print(min(x[1] for x in robot.paint_history))
    print(max(x[1] for x in robot.paint_history))

    
    machine = IntcodeMachine(data)
    machine.start()
    while machine.is_alive():
        # milk the machine:
        milk(machine, robot.process)

        # feed the machine
        if machine.is_alive() and machine.input_blocked.is_set() and machine.input.empty():
            milk(machine, robot.process)
            machine.input_blocked.clear()
            machine.input.put(robot.curr_color())

        time.sleep(0.001)

    # drain the output
    milk(machine, robot.process)
    machine.join()
    
    robot.draw_roof()
    print(min(x[0] for x in robot.paint_history))
    print(max(x[0] for x in robot.paint_history))
    print(min(x[1] for x in robot.paint_history))
    print(max(x[1] for x in robot.paint_history))

    return robot.paint_history
    
    
def phase2(data, machine):
    return run_static(data, [2])


def main():
    global data
    data = [int(x) for x in helper.data_raw("day11.txt").split(",")]

    # print(phase1(data))
    print(phase1(data, 1))


if __name__ == "__main__":
    main()
