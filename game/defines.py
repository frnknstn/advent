#!/usr/bin/python
from __future__ import division
from __future__ import print_function

import pygame.font as pygame_font_module      # avoid polluting other module's namespaces

DEBUG = False
DEBUG = True

BENCHMARK = False
BENCHMARK = True

RESOURCE_CATEGORIES = ("tiles", "interface", "icons", "music", "sound")
SPRITE_DIR = "sprites"
CAP_FPS = None
CAP_FPS = 60
CAP_FPS = 90
CAP_FPS = 300
#CAP_FPS = 999

DOUBLE_SIZE = False
DOUBLE_SIZE = True

SCREEN_SIZE = (64, 64)
REAL_SCREEN_SIZE = (64 * 8, 64 * 8)


DIRECTIONS = ("none", "up", "right", "down", "left")

# functions
def debug(*args):
    if DEBUG:
        print(*args)

def pygame_default_font(size):
    """
    Emulate the output of pygame.font.SysFont("", size), but in a way that doesn't crash if run through cx_Freeze

    :type size: int
    :rtype: pygame.font.Font
    """
    
    # reduce the size to match what pygame's font.c:font_init() does
    # reportedly, this is to keep the modern pygame default font roughly the same size as that of older pygame versions.
    size = int(size * 0.6875)
    if size < 1:
        size = 1
    
    return pygame_font_module.Font("fonts/freesansbold.ttf", size)

# game constants

