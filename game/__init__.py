# coding=utf-8

from __future__ import division
from __future__ import print_function

import sys
import time
from six.moves.queue import Queue, Empty as QueueEmpty

from typing import Optional

import pygame

from fps_counter import FPSCounter

from defines import *

# util functions
# chose the best time function
if sys.platform.startswith('linux'):
    get_time = time.time
else:
    get_time = time.clock


# classes
class Game(object):
    GAME_STATES = {"start", "pre-game", "intro", "main", "post-level", "outro"}

    def __init__(self, screen_size, real_screen_size=None, name="pygame game", cap_fps=CAP_FPS):
        self.state = "start"
        self.running = True

        self.screen = None  # type: Optional[pygame.Surface]
        self._real_screen = None

        self.cap_fps = cap_fps
        self.clock = pygame.time.Clock()
        self.current_time = 0

        self.window_caption = name

        # create our window
        self.set_screen_size(screen_size, real_screen_size)
        pygame.display.set_caption(self.window_caption)

        self.set_state("start")

    def set_screen_size(self, screen_size, real_screen_size=None):
        """
        Set our window resolution, and optionally our scaling

        :type screen_size: tuple[int, int]
        :type real_screen_size:  tuple[int, int] or None
        """
        if real_screen_size is None:
            # No upscaling
            self.screen = pygame.display.set_mode(screen_size)
        else:
            self._real_screen = pygame.display.set_mode(real_screen_size)
            self.screen = pygame.Surface(screen_size)
            self.screen.convert(self._real_screen)

    def flip(self, surface=None):
        """Move the screen surface to the actual display"""
        if surface is None:
            surface = self.screen

        if self._real_screen is not None:
            pygame.transform.scale(surface, self._real_screen.get_size(), self._real_screen)

        pygame.display.flip()

        if CAP_FPS is not None:
            dt = self.clock.tick(CAP_FPS) / 1000
        else:
            # be a good multitasking program buddy
            time.sleep(0.0005)
            dt = self.clock.tick() / 1000

        self.current_time += dt
        return dt

    def set_state(self, state):
        assert (state in self.GAME_STATES)
        debug("Game state changed to '%s'" % state)
        self.state = state

    def parse_events(self, handlers=None):
        """Parse the PyGame event queue, handling global keys, or passing them off to a handler otherwise.

        You can supply multiple handler
        """

        def dummy_handler(event):
            """dummy event handler"""
            return False

        def try_handlers(event):
            """try all our handlers in order, returns True if any of them handled it"""
            handled = False
            for handler in handlers:
                handled = handler(event)
                if handled:
                    return True
            return False

        if handlers is None:
            handlers = (dummy_handler,)
        elif callable(handlers):
            handlers = (handlers,)

        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if not try_handlers(event):
                    if event.key in (pygame.K_ESCAPE,):
                        # default handler just exits on Esc
                        self.running = False
                    else:
                        # unhandled event
                        debug(event)

            elif event.type == pygame.KEYUP:
                try_handlers(event)

            elif event.type == pygame.QUIT:
                self.running = False

    def flash_screen(self, next_state, duration=0.5):
        """Draw a whole-screen flash transition"""
        screen = self.screen

        flash_surface = pygame.Surface(screen.get_size())
        flash_surface.fill((255, 255, 255))

        back_buffer = pygame.Surface(screen.get_size())
        back_buffer.blit(screen, (0, 0))

        frames = 0
        duration = 0.50
        step_duration = duration / 255
        start_time = get_time()

        while self.running:
            self.parse_events()

            screen.blit(back_buffer, (0, 0))

            fade_time = get_time() - start_time
            alpha = min(int(fade_time / step_duration), 255)
            flash_surface.set_alpha(alpha)
            screen.blit(flash_surface, (0, 0))

            self.flip()
            frames += 1

            if fade_time > duration:
                break

        debug("flash_screen() took %d frames for a %f second fade" % (frames, duration))
        self.set_state(next_state)

    def main(self):
        """Example for game main entry point. This should be overridden by any child classes"""
        debug("Game.main() running")

        while self.running:
            if self.state == "start":
                self.flash_screen("main", 0.05)
            elif self.state == "main":
                self.main_game()
            elif self.state == "post-level":
                # post-game interlude
                self.flash_screen("outro")
            elif self.state == "outro":
                self.outro()
                self.running = False

        debug("Leaving main loop")

    def main_game(self):
        """An example of the main game loop. This should be overridden by child classes"""
        fps = FPSCounter()

        # prepare the game

        # Main Loop
        self.flip()
        while self.running:
            # parse events and input
            self.parse_events()

            # move the sprites
            self.handle_sprites()
            # draw the frame
            self.draw_frame()

            # complete the frame
            average_fps = fps.frame()

            if DEBUG:
                # draw debug info
                pygame.display.set_caption(
                    "%s - %d FPS - %0.3f sec" % (self.window_caption, average_fps, self.current_time))

            self.flip()

            # check if our state changed during this frame
            if self.state != "main":
                debug("breaking to state %s" % self.state)
                break

    def outro(self):
        """Put your fancy outro here if you want. This should be overridden by child classes"""
        raise NotImplementedError("outro should be overridden by child")
    
    def handle_sprites(self):
        """Put your main game logic here This should be overridden by child classes"""
        raise NotImplementedError("outro should be overridden by child")
        
    def draw_frame(self):
        """Put your graphics logic here This should be overridden by child classes"""
        raise NotImplementedError("outro should be overridden by child")


def main(game_class=Game):
    try:
        pygame.init()

        my_game = game_class(SCREEN_SIZE, REAL_SCREEN_SIZE, name="Blast!")
        my_game.main()

    finally:
        debug("Stopping pygame...")

        pygame.quit()

    debug("Exiting.")
