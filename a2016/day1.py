import timeit
from typing import List, Tuple, Optional, Dict, Sequence, Iterable, Set, Generator, Callable
from functools import reduce
import itertools
import copy
import re
from time import sleep
import math

from a2016 import helper

data = None


def turn(start_dir: int, turn_dir: int):
    dir = start_dir
    
    if turn_dir == 'L':
        dir -= 1
    elif turn_dir == 'R':
        dir += 1
    else:
        # sanity
        raise Exception("Invalid turn direction")
    
    # normalise
    while dir >= 4:
        dir -= 4
    while dir < 0:
        dir += 4
    
    return dir


def phase1(data: List[str], quiet=False):
    if not quiet:
        print(f"# === Phase 1 with {len(data):d} lines")
        
    pos = [0, 0]
    dir = 0     # (0: N, 1: E, 2: S, 3: W)
    
    for item in (x.strip() for x in data.split(",")):
        turn_dir = item[0].upper()
        distance = int(item[1:])
        
        dir = turn(dir, turn_dir)
        
        # move based on direction
        if dir == 0:
            # north
            pos[1] += distance
        elif dir == 1:
            # east
            pos[0] += distance
        elif dir == 2:
            # south
            pos[1] -= distance
        elif dir == 3:
            # west
            pos[0] -= distance
        else:
            # sanity
            raise Exception("Invalid direction")
        
    return helper.manhattan(pos)


def main():
    global data
    data = helper.data_raw("day1.txt")

    test_data = """\
R5, L4, R5, R3
""".strip()    

    print(phase1(test_data[:]))
    print(phase1(data[:]))
    # print(phase1(test_data[:], calc_func=calc_part_rpn))
    # print(phase1(data[:], calc_func=calc_part_rpn))


    # print(phase1(parse_data(test_data[:], d=4), steps=6, d=4))
    # print(phase1(parse_data(data[:], d=4), steps=6, d=4))

    # print("timed")
    # print(timeit.timeit('target = phase1(data)', number=100, globals=globals()))
    # print(timeit.timeit('phase1(data)', setup='target = phase1(data)', number=100, globals=globals()))
    # print(timeit.timeit('phase2(data, *target)', setup='target = phase1(data)', number=100, globals=globals()))


if __name__ == "__main__":
    main()
