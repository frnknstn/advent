import sys
import timeit
from typing import List, Tuple, Optional, Dict, Sequence, Iterable, Set, Generator, Callable
import collections
import re
from math import pow, sqrt, ceil, floor
from enum import Enum, IntEnum
import functools
import itertools


from a2023 import helper


class Map:
    def __init__(self, data):
        self.data = data
        self.width = len(data[0])
        self.height = len(data)
    
    def __getitem__(self, coords: Tuple[int, int])->str:
        return self.data[coords[1]][coords[0]]
    
    def flood(self, seed: Iterable, bound=Iterable)->Set:
        width = self.width
        height = self.height
        
        filled = set()
        check = set(seed)
        bound = set(bound)
        
        while len(check) > 0:
            coords = check.pop()
            
            if coords in bound:
                continue
            if coords[0] < 0 or coords[0] >= width:
                continue
            if coords[1] < 0 or coords[1] >= height:
                continue
            if coords in filled:
                continue
            
            # checks out, add it and check the orthogonals
            filled.add(coords)
            x, y = coords
            check.update((
                (x-1, y), (x, y-1), (x+1, y), (x, y+1)
            ))
        return filled
    
    def display(self, filter_func=None):
        for y, line in enumerate(self.data):
            if not filter_func:
                print(line)
            else:
                print("".join((c if filter_func((x, y)) else ' ') for x, c in enumerate(line)))


def space_pad(map: Map)->Map:
    data = []
    
    for row in map.data:
        if '#' not in row:
            data.append(row)
        data.append(row)
    
    double_cols = []
    for x in range(map.width):
        found = False
        for y in range(len(data)):
            if data[y][x] == '#':
                found = True
                break
        if not found:
            double_cols.append(x)
    
    # add the new cols
    retval = []
    for row in data:
        new_row = []        
        for x, c in enumerate(row):
            if x in double_cols:
                new_row.append(c)
            new_row.append(c)
        retval.append("".join(new_row))                
                
    return Map(retval)


def find_gaps(map: Map)->Tuple[List[int], List[int]]:
    data = map.data[:]
    
    double_cols: List[int] = []
    double_rows: List[int] = []
    for x in range(map.width):
        found = False
        for y in range(len(data)):
            if data[y][x] == '#':
                found = True
                break
        if not found:
            double_cols.append(x)
            
    for y, row in enumerate(data):
        if '#' not in row:
            double_rows.append(y)
    
    return (double_cols, double_rows)


def phase1(data: List[str], quiet=False, **kwargs):
    if not quiet:
        print(f"# === Phase 1 with {len(data):d} lines")

    total = 0
    
    map = Map(data)
    map = space_pad(map)
    
    stars = set()
    for y, row in enumerate(map.data):
        for x, c in enumerate(row):
            if c == '#':
                stars.add((x, y))
    
    map.display()
    print(stars)
    
    for a, b in itertools.combinations(sorted(stars), 2):
        total += helper.manhattan(a, b)

    print("result", total)


def phase2(data: List[str], quiet=False, **kwargs):
    if not quiet:
        print(f"# === Phase 2 with {len(data):d} lines")
    total = 0
    
    map = Map(data)
    double_cols, double_rows = find_gaps(map)
    print(double_cols, double_rows)
    
    print("padded")
    stars = set()
    for y, row in enumerate(map.data):
        offset_y = len([i for i in double_rows if y > i])
        for x, c in enumerate(row):
            offset_x = len([i for i in double_cols if x > i])
            if c == '#':
                stars.add((x + (offset_x*999999), y + (offset_y*999999)))
    
    print(stars)
    
    for a, b in itertools.combinations(sorted(stars), 2):
        total += helper.manhattan(a, b)

    print("result", total)





def main():
    data = helper.data_list(helper.guess_data_file(__file__))

    test = """
...#......
.......#..
#.........
..........
......#...
.#........
.........#
..........
.......#..
#...#.....
""".strip().split('\n')

    # phase1(test)
    # phase1(data)
    phase2(test)
    phase2(data)


if __name__ == "__main__":
    main()
