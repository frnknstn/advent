import sys
import timeit
from typing import List, Tuple, Optional, Dict, Sequence, Iterable, Set, Generator, Callable
import collections
import re
from math import pow, sqrt, ceil, floor
from enum import Enum

from a2023 import helper


def ceilplus(x)->int:
    retval = ceil(x)
    if retval == x:
        retval += 1
    return retval

def floorminus(x)->int:
    retval = floor(x)
    if retval == x:
        retval -= 1
    return retval

    

def phase1(data: List[str], quiet=False, **kwargs):
    if not quiet:
        print(f"# === Phase 1 with {len(data):d} lines")

    total = 1

    races = [(int(x), int(y)) for x, y in zip(data[0].split()[1:],data[1].split()[1:])] 
    print(races)

    # x = hold time
    # t = total time
    # y = distance
    # g = goal

    # y = x * (t - x)
    # y = tx - x^2

    for time, goal in races:
        print("race:", time, goal, 2*sqrt(goal))
        min_x = 0.5 * (time - sqrt(time**2 - (4 * goal)))
        max_x = 0.5 * (sqrt(time ** 2 - (4 * goal)) + time)
        low, high = ceilplus(min_x), floorminus(max_x)
        print(min_x, low, high, max_x)
        total *= high - low + 1
        
    print("result", total)



def phase2(data: List[str], quiet=False, **kwargs):
    if not quiet:
        print(f"# === Phase 2 with {len(data):d} lines")

    total = 1

    races = [(
        int(data[0].split(":")[1].replace(" ", "")),
        int(data[1].split(":")[1].replace(" ", ""))
    )]
        
    print(races)

    # x = hold time
    # t = total time
    # y = distance
    # g = goal

    # y = x * (t - x)
    # y = tx - x^2

    for time, goal in races:
        print("race:", time, goal, 2 * sqrt(goal))
        min_x = 0.5 * (time - sqrt(time ** 2 - (4 * goal)))
        max_x = 0.5 * (sqrt(time ** 2 - (4 * goal)) + time)
        low, high = ceilplus(min_x), floorminus(max_x)
        print(min_x, low, high, max_x)
        total *= high - low + 1

    print("result", total)


def main():
    data = helper.data_list(helper.guess_data_file(__file__))

    test = """Time:      7  15   30
Distance:  9  40  200""".strip().split('\n')

    phase1(test)
    phase1(data)
    phase2(test)
    phase2(data)


if __name__ == "__main__":
    main()
