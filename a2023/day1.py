import timeit
from typing import List, Tuple, Optional, Dict, Sequence, Iterable, Set, Generator, Callable
import re

from a2023 import helper

data = None


def phase1(data: List[str], quiet=False):
    if not quiet:
        print(f"# === Phase 1 with {len(data):d} lines")
    
    total = 0    
    for line in data:
        if not line:
            continue
        numbers = re.findall(r'[0-9]', line)
        total += int(numbers[0] + numbers[-1]) 
    
    print(total)


def phase2(data: List[str], quiet=False):
    if not quiet:
        print(f"# === Phase 2 with {len(data):d} lines")

    def parse(s: str)->str:
        if s == 'one':
            return '1'
        elif s == 'two':
            return '2'
        elif s == 'three':
            return '3'
        elif s == 'four':
            return '4'
        elif s == 'five':
            return '5'
        elif s == 'six':
            return '6'
        elif s == 'seven':
            return '7'
        elif s == 'eight':
            return '8'
        elif s == 'nine':
            return '9'
        else:
            return s

    def match(regex: str, s: str):
        """get overlapping re matches"""
        retval = []
        for i in range(len(s)):
            match = re.match(regex, s[i:])
            if not match:
                continue
            retval.append(match[0])
        return retval

    total = 0
    for line in data:
        if not line:
            continue
        numbers = match(r'one|two|three|four|five|six|seven|eight|nine|[0-9]', line)
        total += int(parse(numbers[0]) + parse(numbers[-1]))
        print(line, numbers, parse(numbers[0]), parse(numbers[-1]), int(parse(numbers[0]) + parse(numbers[-1])), total)

    print(total)


def main():
    global data
    data = helper.data_list("day1.txt")

    test = """two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen""".split()

    phase1(data)
    phase2(data)
    phase2(test)


if __name__ == "__main__":
    main()
