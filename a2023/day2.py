import timeit
from typing import List, Tuple, Optional, Dict, Sequence, Iterable, Set, Generator, Callable
import collections
import re

from a2023 import helper

data = None


class Bag(collections.namedtuple('Bag', "r g b")):
    __slots__ = ()
    def set_max(self, r, g, b):
        new = Bag(
            max(r, self.r),
            max(g, self.g),
            max(b, self.b)
        )
        return new
    
    @property
    def power(self)->int:
        return self.r * self.g * self.b


def phase1(data: List[str], quiet=False, **kwargs):
    if not quiet:
        print(f"# === Phase 1 with {len(data):d} lines")
        
    limit = kwargs["limit"]
        
    def fit(r, g, b, limit:dict)->bool:
        """is this a valid fit"""
        if r > limit['r']:
            return False
        elif g > limit['g']:
            return False
        elif b > limit['b']:
            return False        
        return True
    
    total = 0
    for line in data:
        if not line:
            continue
        game_id, game_data = re.match(r"^Game (\d+): (.*)$", line).groups()
        game_id = int(game_id)
        rounds = game_data.split(';')
        valid_game = True
        for game_round in rounds:
            r, g, b = 0, 0, 0
            game_round = game_round.strip()
            for color in game_round.split(','):
                color = color.strip()
                n, c = color.split(" ")
                if c == "red":
                    r += int(n)
                elif c == "green":
                    g += int(n)
                elif c == "blue":
                    b += int(n)
            if not fit(r, g, b, limit):
                print("FAIL", game_id, r, g, b)
                valid_game = False
                break
                
        if valid_game:
            total += game_id
        
    print(total)


def phase2(data: List[str], quiet=False, **kwargs):
    if not quiet:
        print(f"# === Phase 2 with {len(data):d} lines")

    total = 0
    
    for line in data:
        if not line:
            continue
        game_id, game_data = re.match(r"^Game (\d+): (.*)$", line).groups()
        game_id = int(game_id)
        rounds = game_data.split(';')
        
        min_bag = Bag(0, 0, 0)
        for game_round in rounds:
            r, g, b = 0, 0, 0
            game_round = game_round.strip()
            for color in game_round.split(','):
                color = color.strip()
                n, c = color.split(" ")
                if c == "red":
                    r += int(n)
                elif c == "green":
                    g += int(n)
                elif c == "blue":
                    b += int(n)
            min_bag = min_bag.set_max(r, g, b)

        total += min_bag.power

    print(total)


def main():
    global data
    data = helper.data_list("day2.txt")

    test = """Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green""".split('\n')

    #12 red cubes, 13 green cubes, and 14 blue cubes?
    limit = {"r": 12, "g": 13, "b": 14}
    
    phase1(data, limit=limit)
    phase2(data, limit=limit)


if __name__ == "__main__":
    main()
