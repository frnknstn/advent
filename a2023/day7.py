import sys
import timeit
from typing import List, Tuple, Optional, Dict, Sequence, Iterable, Set, Generator, Callable
import collections
import re
from math import pow, sqrt, ceil, floor
from enum import Enum, IntEnum

from a2023 import helper


class HandType(IntEnum):
    HIGH = 0
    ONE_PAIR = 1
    TWO_PAIR = 2
    THREE = 3
    FULL = 4
    FOUR = 5
    FIVE = 6

# abuse lexical sorting to rank the cards in hand
card_normal_trans = str.maketrans({'T': 'a', 'J': 'b', 'Q': 'c', 'K': 'd', 'A': 'e'})
card_wild_normal_trans  = str.maketrans({'T': 'a', 'J': '1', 'Q': 'c', 'K': 'd', 'A': 'e'})


class Hand:
    def __init__(self, cards, bid):
        self.cards = cards
        self.bid = bid

        self.counted = collections.Counter(cards)

        self.hand_type = self.calc_hand_type(self.counted)
        self.card_normal = cards.translate(card_normal_trans)

    @staticmethod
    def calc_hand_type(counted):
        counts = counted.values()
        if 5 in counts:
            return HandType.FIVE
        elif 4 in counts:
            return HandType.FOUR
        elif 3 in counts:
            if 2 in counts:
                return HandType.FULL
            else:
                return HandType.THREE
        elif 2 in counts:
            if len(counts) == 3:
                return HandType.TWO_PAIR
            else:
                return HandType.ONE_PAIR
        else:
            return HandType.HIGH

    @property
    def key(self)->Tuple:
        return (self.hand_type, self.card_normal)

    def __repr__(self):
        return repr((self.cards, self.bid, self.counted, self.hand_type, self.key))

class WildHand(Hand):
    def __init__(self, cards, bid):
        super().__init__(cards, bid)
        self.card_normal = cards.translate(card_wild_normal_trans)

    @staticmethod
    def calc_hand_type(counted):
        if 'J' not in counted:
            # no wild cards
            return Hand.calc_hand_type(counted)
        
        jacks = counted['J']
        counted = counted.copy()
        del counted['J']
        counts = counted.values()
        
        if 5 in counts:
            return HandType.FIVE
        elif 4 in counts:
            return HandType.FIVE
        elif 3 in counts:
            if jacks == 1:
                return HandType.FOUR
            else:
                return HandType.FIVE
        elif 2 in counts:
            if jacks == 1:
                if len(counts) == 2:
                    return HandType.FULL
                else:
                    return HandType.THREE
            elif jacks == 2:
                return HandType.FOUR
            else:
                return HandType.FIVE
        else:
            if jacks == 1:
                return HandType.ONE_PAIR
            elif jacks == 2:
                return HandType.THREE
            elif jacks == 3:
                return HandType.FOUR
            elif jacks == 4:
                return HandType.FIVE
            elif jacks == 5:
                return HandType.FIVE
        raise Exception


def phase1(data: List[str], quiet=False, **kwargs):
    if not quiet:
        print(f"# === Phase 1 with {len(data):d} lines")

    total = 0

    hands = []
    for line in data:
        cards, bid = line.split()
        bid = int(bid)
        hand = Hand(cards, bid)
        hands.append(hand)

    for rank, hand in enumerate(sorted(hands, key=lambda x: x.key)):
        print(hand)
        total += hand.bid * (1+rank)

    print("result", total)



def phase2(data: List[str], quiet=False, **kwargs):
    if not quiet:
        print(f"# === Phase 2 with {len(data):d} lines")

    total = 0

    hands = []
    for line in data:
        cards, bid = line.split()
        bid = int(bid)
        hand = WildHand(cards, bid)
        hands.append(hand)

    for rank, hand in enumerate(sorted(hands, key=lambda x: x.key)):
        print(hand)
        total += hand.bid * (1+rank)

    print("result", total)



def main():
    data = helper.data_list(helper.guess_data_file(__file__))

    test = """32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483""".strip().split('\n')

    phase1(test)
    phase1(data)
    phase2(test)
    phase2(data)


if __name__ == "__main__":
    main()
