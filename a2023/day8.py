import sys
import timeit
from typing import List, Tuple, Optional, Dict, Sequence, Iterable, Set, Generator, Callable
import collections
import re
from math import pow, sqrt, ceil, floor
from enum import Enum, IntEnum
from functools import reduce


from a2023 import helper


class Node:
    __slots__ = ('name', 'left', 'right')
    def __init__(self, name: str, left: ('Node', str), right: ('Node', str)):
        self.name = name
        self.left = left
        self.right = right
    
    def __repr__(self):
        try:
            left_name = self.left.name
        except ValueError:
            left_name = self.left
        try:
            right_name = self.right.name
        except ValueError:
            right_name = self.right

        return f'<Node {self.name}: {left_name}, {right_name}>'
    
    
def phase1(data: List[str], quiet=False, **kwargs):
    if not quiet:
        print(f"# === Phase 1 with {len(data):d} lines")

    total = 0
    
    nodes = {}
    commands = data[0]
    for line in data[2:]:
        name, left, right = re.match(r'(...) = \((...), (...)\)', line).groups()
        node = Node(name, left, right)
        nodes[name] = node
    
    # reassign the node names to actual nodes
    for name, node in nodes.items():
        node.left = nodes[node.left]
        node.right = nodes[node.right]
            
    location = nodes['AAA']
    while location.name != 'ZZZ':
        for command in commands:
            if command == 'L':
                location = location.left
            else:
                location = location.right
            
            total += 1
            
            if location.name == 'ZZZ':
                break
                
    print("result", total)



def phase2(data: List[str], quiet=False, **kwargs):
    if not quiet:
        print(f"# === Phase 2 with {len(data):d} lines")

    total = 0

    nodes = {}
    commands = data[0]
    for line in data[2:]:
        name, left, right = re.match(r'(...) = \((...), (...)\)', line).groups()
        node = Node(name, left, right)
        nodes[name] = node

    # reassign the node names to actual nodes
    for name, node in nodes.items():
        node.left = nodes[node.left]
        node.right = nodes[node.right]

    
    locations = [v for k, v in nodes.items() if k.endswith('A')]
    print(locations)

    ttgoal = [1] * len(locations)

    def done():
        for tt in ttgoal:
            if tt <= 1:
                return False
        return True
    
    while not done():
        for command in commands:
            if command == 'L':
                locations = [x.left for x in locations]
            else:
                locations = [x.right for x in locations]
            total += 1
            
            # check for goal / loop
            for i, location in enumerate(locations):
                if ttgoal[i] == 1 and location.name.endswith('Z'):
                    ttgoal[i] = total
                
            if done():
                break

    print(reduce(helper.lcm, ttgoal))
    print("result", total)



def main():
    data = helper.data_list(helper.guess_data_file(__file__))

    test = """RL

AAA = (BBB, CCC)
BBB = (DDD, EEE)
CCC = (ZZZ, GGG)
DDD = (DDD, DDD)
EEE = (EEE, EEE)
GGG = (GGG, GGG)
ZZZ = (ZZZ, ZZZ)""".strip().split('\n')

    phase1(test)
    phase1(data)
    # phase2(test)
    phase2(data)


if __name__ == "__main__":
    main()
