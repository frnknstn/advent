import sys
import timeit
from typing import List, Tuple, Optional, Dict, Sequence, Iterable, Set, Generator, Callable
import collections
import re
from math import pow
from enum import Enum

from a2023 import helper

MAX_RANGE_END: int = 0

class MapRange:
    def __init__(self, line):
        dest, src, size = (int(x) for x in line.strip().split())
        self.size = size
        
        self.src_start = src
        self.src_end = src + size
        
        self.dest_start = dest
        self.dest_end = dest + size
    
    def in_range(self, x:int)->bool:
        """return true if x is in our source map range"""
        return (x >= self.src_start) and (x < self.src_end)
    
    def map(self, x:int)->int:
        """return the value mapped to the dest range (including our end)"""
        if x < self.src_start or x > self.src_end:
            raise ValueError(f"{x} is not in {self.src_start} <= {x} <= {self.src_end}")
        
        offset = x - self.src_start
        return self.dest_start + offset
    
    def __repr__(self):
        return f"<MapRange [{self.src_start}-{self.src_end})->{self.dest_start} ({self.size})>"
        

def parse_data(data: List[str])->Tuple[List[int], List[List[MapRange]]]:
    global MAX_RANGE_END
    layers = []
    curr_layer: List[MapRange] = []
    seeds: List[int] = []
    
    for line in data:
        if not line:
            continue
        if line.startswith("seeds: "):
            seeds = [int(x.strip()) for x in line.split()[1:]]
            continue
        
        if ':' in line:
            # new category
            if curr_layer:
                layers.append(curr_layer)
                curr_layer = []
            continue
        
        map_range = MapRange(line)
        curr_layer.append(MapRange(line))
        MAX_RANGE_END = max(MAX_RANGE_END, map_range.dest_end, map_range.src_end)

    layers.append(curr_layer)
    
    return seeds, layers


def phase1(data: List[str], quiet=False, **kwargs):
    if not quiet:
        print(f"# === Phase 1 with {len(data):d} lines")
       
    seeds, layers = parse_data(data)
    
    locations = []
    
    for x in seeds:
        for layer in layers:
            for map_range in layer:
                if map_range.in_range(x):
                    x = map_range.map(x)
                    break
        locations.append(x)

    print("min", min(locations))


def recurse_range(start:int, end:int, layers: List[List[MapRange]])->int:
    """find all the ranges in our sorted layer and recurse, returning our min location"""

    if len(layers) == 0:
        # base case
        return start
    elif len(layers) == 1:
        this_layer = layers[0]
        sub_layers = []
    else:
        this_layer = layers[0]
        sub_layers = layers[1:]
    
    min_location = MAX_RANGE_END
    x = start
    
    for map_range in this_layer:
        if map_range.src_end <= start:
            # before our time
            continue

        if map_range.src_start >= end:
            # no more layers in our range
            break
        
        if x < map_range.src_start:
            # we have a gap, null map
            location = recurse_range(x, map_range.src_start, sub_layers)
            min_location = min(min_location, location)
            # advance to start
            x = map_range.src_start
        
        # we are now in the range
        x_end = min(map_range.src_end, end)
        location = recurse_range(map_range.map(x), map_range.map(x_end), sub_layers)
        min_location = min(min_location, location)
        x = x_end
        
        if x >= end:
            # we are done
            return min_location
    
    if x < end:
        # trailing gap, more null map
        location = recurse_range(x, end, sub_layers)
        min_location = min(min_location, location)
    
    # we are done
    return min_location


def phase2r(data: List[str], quiet=False, **kwargs):
    if not quiet:
        print(f"# === Phase 2 with {len(data):d} lines")

    seeds, layers = parse_data(data)

    min_location = MAX_RANGE_END
    
    # convert seeds to ranges
    seed_ranges = [(int(i), int(i+j)) for i, j in zip(seeds[::2], seeds[1::2])]
    
    # sort layers
    for layer in layers:
        layer.sort(key=lambda x: (x.src_start, x.size, x.dest_start))
        print(layer)

    # do the lookups
    for seed_range in seed_ranges:
        print(seed_range)
        location = recurse_range(seed_range[0], seed_range[1], layers)
        min_location = min(location, min_location)

    print("min", min_location)


def phase2(data: List[str], quiet=False, **kwargs):
    if not quiet:
        print(f"# === Phase 2 with {len(data):d} lines")

    total = 0

    seeds, layers = parse_data(data)

    min_location = 0
    
    # convert seeds to ranges
    seed_ranges = [range(i, i+j) for i, j in zip(seeds[::2], seeds[1::2])]

    # do the lookups
    for seed_range in seed_ranges:
        print(seed_range)
        for x in seed_range:
            # print(f"seed: {x}")
            total += 1
            if total % 100000 == 0:
                print(x)
            for layer in layers:
                for map_range in layer:
                    if map_range.in_range(x):
                        x = map_range.map(x)
                        # print(x)
                        break
            if min_location == 0 or x < min_location:
                min_location = x

    print("min", min_location)
    print("total", total)


def main():
    data = helper.data_list(helper.guess_data_file(__file__))

    test = """seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4""".strip().split('\n')

    phase1(test)
    phase1(data)
    phase2(test)
    # phase2(data)
    phase2r(test)
    phase2r(data)


if __name__ == "__main__":
    main()
