from typing import Tuple, List, Iterable, Optional
import os
import math


module = "a2023"

def guess_data_file(hint:str)->str:
    """Guess what the data file will be called, based on the __file__ passed in
    
    Example:
        (from day2.py)
        >>> helper.guess_data_file(__file__)
        day2.txt
    """
    if not hint:
        raise ValueError("No hint supplied")
    head, tail = os.path.splitext(os.path.basename(hint))
    return head + ".txt"


def data_raw(filename):
    with open(os.path.join(module, filename), "rt") as fp:
        return fp.read()


def data_list(filename, strip=True) -> List:
    with open(os.path.join(module, filename), "rt") as fp:
        data = fp.read()
        if strip:
            data = data.strip()
        return data.split("\n")


def manhattan(pos: Tuple[int, int], target: Optional[Tuple[int, int]]=(0, 0)):
    """returns the Manhattan distance"""
    return abs(pos[0]-target[0]) + abs(pos[1]-target[1])


try:
    from math import lcm
except ImportError:
    def lcm(a: int, b: int) -> int:
        """returns the lowest common multiple of two numbers"""
        return abs(a * b) // int(math.gcd(a, b))

try:
    from itertools import pairwise
except ImportError:
    def pairwise(s: Iterable) -> Iterable:
        "s -> (s0,s1), (s1,s2), (...)"
        s = iter(s)
        a = next(s)
        while True:
            try:
                b = next(s)
            except StopIteration:
                return 
            yield (a, b)
            a = b
            
        
