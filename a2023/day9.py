import sys
import timeit
from typing import List, Tuple, Optional, Dict, Sequence, Iterable, Set, Generator, Callable
import collections
import re
from math import pow, sqrt, ceil, floor
from enum import Enum, IntEnum
import functools


from a2023 import helper

    
def phase1(data: List[str], quiet=False, **kwargs):
    if not quiet:
        print(f"# === Phase 1 with {len(data):d} lines")

    total = 0
    prediction = 0
    
    for line in data:
        seq = [int(x) for x in line.split()]
        if kwargs.get('reversed', True):
            seq.reverse()
        
        # build the pyramid
        pyramid = [seq]
        while True:
            seq = pyramid[-1]
            diff = [y-x for x, y in helper.pairwise(seq)]
            pyramid.append(diff)
            
            # exit condition
            if all(x == 0 for x in diff):
                break
        
        # calculate forward
        pyramid.reverse()
        last = 0
        
        for seq in pyramid:
            prediction = last + seq[-1]
            seq.append(prediction)
            last = prediction
        
        print(prediction, pyramid)
        total += prediction
                
    
    print("result", total)



def phase2(data: List[str], quiet=False, **kwargs):
    if not quiet:
        print(f"# === Phase 2 with {len(data):d} lines")

    phase1(data, quiet=True, reversed=True)
    return
    
    total = 0

    print("result", total)



def main():
    data = helper.data_list(helper.guess_data_file(__file__))

    test = """0 3 6 9 12 15
1 3 6 10 15 21
10 13 16 21 30 45""".strip().split('\n')

    # phase1(test)
    # phase1(data)
    phase2(test)
    phase2(data)


if __name__ == "__main__":
    main()
