import timeit
from typing import List, Tuple, Optional, Dict, Sequence, Iterable, Set, Generator, Callable
import collections
import re

from a2023 import helper

data = None


def find_adjacent_symbol(x1, x2, y, data) -> bool:
    height = len(data)
    width = len(data[0])
    
    def sample(x, y, data):
        if x < 0 or x >= width:
            return False
        if y < 0 or y >= height:
            return False
        return data[y][x] not in ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.')

    for i in range(x1, x2):
        if sample(i - 1, y - 1, data):
            return True
        if sample(i, y - 1, data):
            return True
        if sample(i + 1, y - 1, data):
            return True

        if sample(i - 1, y, data):
            return True
        if sample(i + 1, y, data):
            return True

        if sample(i - 1, y + 1, data):
            return True
        if sample(i, y + 1, data):
            return True
        if sample(i + 1, y + 1, data):
            return True
    return False

def phase1(data: List[str], quiet=False, **kwargs):
    if not quiet:
        print(f"# === Phase 1 with {len(data):d} lines")
    
    height = len(data)
    width = len(data[0])
    
    total = 0
    
    for y in range(height):
        row = data[y]
        for match in re.finditer(r"\d+", row):
            if find_adjacent_symbol(match.start(), match.end(), y, data):
                total += int(match.group(0))
            
    print(total)

class Span:
    def __init__(self, x, y, data):
        self.start = 0
        self.end = len(data)
        self.value:int = 0

        self.y = y
        self.x = x
        
        self.extract(data)
        
    def extract(self, data):
        row = data[self.y]
        
        # scan left
        x = self.x - 1
        while x >= 0:
            if row[x] not in "0123456789":
                self.start = x + 1
                break
            x -= 1
        
        # scan right
        x = self.x + 1
        while x < len(row):
            if row[x] not in "0123456789":
                self.end = x
                break
            x += 1
        
        self.value = int(row[self.start:self.end])
    
    @property
    def ident(self):
        return (self.y, self.start, self.end)
    
    def __eq__(self, other):
        return self.ident == other.ident 
    
    def __repr__(self):
        return f"<Span({self.value}, x={self.start}-{self.end}, y={self.y}>"
    
    

def find_adjacent_gears(x, y, data) -> Tuple[int, int]:
    height = len(data)
    width = len(data[0])

    def sample(x, y, data):
        if x < 0 or x >= width:
            return False
        if y < 0 or y >= height:
            return False
        return data[y][x] in ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9')
    
    # where are the numbers
    candidates = []
    for offset in (
            (-1, -1),
            (0, -1),
            (1, -1),
            (-1, 0),
            (1, 0),
            (-1, 1),
            (0, 1),
            (1, 1)
    ):
        sample_pos = (x + offset[0], y + offset[1])
        if sample(*sample_pos, data):
            candidates.append(sample_pos)
    print(candidates)
    
    # find number spans / deduplicate
    spans = []
    for candidate in candidates:
        span = Span(*candidate, data)
        if span not in spans:
            spans.append(span)
    print(spans)
    
    # is it a gear:
    if len(spans) != 2:
        return (0, 0)
    else:
        return (spans[0].value, spans[1].value)
            


def phase2(data: List[str], quiet=False, **kwargs):
    if not quiet:
        print(f"# === Phase 2 with {len(data):d} lines")

    height = len(data)
    width = len(data[0])

    total = 0

    for y in range(height):
        row = data[y]
        for match in re.finditer(r"\*", row):
            gears = find_adjacent_gears(match.start(), y, data)
            if gears != (0, 0):
                total += gears[0] * gears[1]

    print(total)
    
def main():
    global data
    data = helper.data_list("day3.txt")

    test = """467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..""".split('\n')

    phase1(data)
    phase2(data)


if __name__ == "__main__":
    main()
