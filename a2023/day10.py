import sys
import timeit
from typing import List, Tuple, Optional, Dict, Sequence, Iterable, Set, Generator, Callable
import collections
import re
from math import pow, sqrt, ceil, floor
from enum import Enum, IntEnum
import functools
import itertools


from a2023 import helper


class Map:
    def __init__(self, data):
        self.data = data
        self.width = len(data[0])
        self.height = len(data)
    
    def __getitem__(self, coords: Tuple[int, int])->str:
        return self.data[coords[1]][coords[0]]
    
    def flood(self, seed: Iterable, bound=Iterable)->Set:
        width = self.width
        height = self.height
        
        filled = set()
        check = set(seed)
        bound = set(bound)
        
        while len(check) > 0:
            coords = check.pop()
            
            if coords in bound:
                continue
            if coords[0] < 0 or coords[0] >= width:
                continue
            if coords[1] < 0 or coords[1] >= height:
                continue
            if coords in filled:
                continue
            
            # checks out, add it and check the orthogonals
            filled.add(coords)
            x, y = coords
            check.update((
                (x-1, y), (x, y-1), (x+1, y), (x, y+1)
            ))
        return filled
    
    def display(self, filter_func=None):
        for y, line in enumerate(self.data):
            if not filter_func:
                print(line)
            else:
                print("".join((c if filter_func((x, y)) else ' ') for x, c in enumerate(line)))


class Pipe:
    def __init__(self, map: Map, coords, a: Optional['Pipe'], b: Optional['Pipe']):
        self.coords = coords
        self.symbol = map[coords]
        self.a = a
        self.b = b
        
    def __repr__(self):
        return f"<Pipe '{self.symbol}' at {self.coords}>"
    
    def find_start_neighbor_coords(self, map: Map)->List[Tuple[int, int]]:
        width = map.width
        height = map.height
        
        # find the neighbors for the start
        neighbors = []
        coords = self.coords
        
        for direction, connections in (
            ((coords[0]-1, coords[1]), ('-', 'L', 'F')),  # <-S
            ((coords[0], coords[1]-1), ('|', '7', 'F')),  #   S^
            ((coords[0]+1, coords[1]), ('-', '7', 'J')),  #   S->
            ((coords[0], coords[1]+1), ('|', 'L', 'J')),  #   Sv
        ):
            # OOB
            if not (0 <= direction[0] < (width)):
                continue
            if not (0 <= direction[1] < (height)):
                continue
            
            if map[direction] in connections:
                neighbors.append(direction)
        
        assert len(neighbors) == 2
        return neighbors
    
    
    def find_neighbor_coords(self, map: Map)->List[Tuple[int, int]]:
        if self.symbol == 'S':
            return self.find_start_neighbor_coords(map)

        coords = self.coords
        
        if self.symbol == '|':
            return [
                (coords[0], coords[1]-1),
                (coords[0], coords[1]+1),
            ]
        if self.symbol == '-':
            return [
                (coords[0]-1, coords[1]),
                (coords[0]+1, coords[1]),
            ]
        if self.symbol == 'L':
            return [
                (coords[0], coords[1]-1),
                (coords[0]+1, coords[1]),
            ]
        if self.symbol == 'J':
            return [
                (coords[0], coords[1]-1),
                (coords[0]-1, coords[1]),
            ]
        if self.symbol == 'F':
            return [
                (coords[0]+1, coords[1]),
                (coords[0], coords[1]+1),
            ]
        if self.symbol == '7':
            return [
                (coords[0]-1, coords[1]),
                (coords[0], coords[1]+1),
            ]
        else:
            raise Exception(f"Unknown symbol '{self.symbol}'")
    
    @property
    def flow_left(self)->bool:
        return self.a.coords[0] > self.b.coords[0]
    @property
    def flow_right(self)->bool:
        return self.a.coords[0] < self.b.coords[0]
    @property
    def flow_up(self)->bool:
        return self.a.coords[1] > self.b.coords[1]
    @property
    def flow_down(self)->bool:
        return self.a.coords[1] < self.b.coords[1]
    
    def get_lr(self)->Tuple[List[Tuple[int, int]], List[Tuple[int, int]]]:
        """Get the coords of the parts to the left or right of this pipe"""
        # must have neighbors
        assert(self.a is not None)
        assert(self.b is not None)
        
        # work out our flow direction
        x, y = self.coords
        left = []
        right = []
        
        if self.flow_left:
            left.append((x, y+1))
            right.append((x, y-1))
        elif self.flow_right:
            left.append((x, y-1))
            right.append((x, y+1))
            
        if self.flow_up:
            left.append((x-1, y))
            right.append((x+1, y))
        elif self.flow_down:
            left.append((x+1, y))
            right.append((x-1, y))
        
        return (left, right)

        
def phase1(data: List[str], quiet=False, **kwargs):
    if not quiet:
        print(f"# === Phase 1 with {len(data):d} lines")

    total = 0
    
    map = Map(data)

    start_y = -1
    start_x = -1
    for start_y, line in enumerate(data):
        start_x = line.find('S')
        if start_x != -1:
            break

    start_coords = (start_x, start_y)
    print("start", start_coords)

    # find the neighbors for the start
    start = Pipe(map, start_coords, None, None)
    neighbors = start.find_neighbor_coords(map)
    
    print("start neighbors:", neighbors)
    assert len(neighbors) == 2

    # track the pipe
    start = Pipe(map, start_coords, None, None)
    next_coords = neighbors[0]
    pipe = start
    line = [start]
    
    while True:
        # make the next pipe
        next_pipe = Pipe(map, next_coords, pipe, None) 
        pipe.b = next_pipe
        line.append(next_pipe)
        
        # work out where the exit is
        last_coords = pipe.coords
        pipe = next_pipe
        neighbors = next_pipe.find_neighbor_coords(map)
        if neighbors[0] != last_coords:
            next_coords = neighbors[0]
        else:
            next_coords = neighbors[1]

        # have we looped?
        if next_coords == start_coords:
            pipe.b = start
            start.a = pipe
            break
            
    total = len(line)//2
    print("result", total)
    
    return map, line



def phase2(data: List[str], quiet=False, **kwargs):
    if not quiet:
        print(f"# === Phase 2 with {len(data):d} lines")

    total = 0
    
    map, line = phase1(data, quiet=True)
    line_set = set(p.coords for p in line)
    
    # get all the edge coords
    left = set()
    right = set()

    for p in line:
        l, r = p.get_lr()
        left.update(l)
        right.update(r)
    
    left.difference_update(line_set)
    right.difference_update(line_set)
    
    # flood fill each side using the pipe as the boundary
    left = map.flood(seed=left, bound=line_set)
    right = map.flood(seed=right, bound=line_set)
    
    print(len(left), left)
    print(len(right), right)
    
    # guess in and out
    if (0, 1) not in left:
        inner = left
        total = len(left)
    else:
        inner = right
        total = len(right)

    line_set = set(x.coords for x in line)
    map.display(filter_func=lambda c: (c in inner) or (c in line_set))

    print("result", total)



def main():
    data = helper.data_list(helper.guess_data_file(__file__))

    test = """
..........
.S------7.
.|F----7|.
.||OOOO||.
.||OOOO||.
.|L-7F-J|.
.|II||II|.
.L--JL--J.
..........
""".strip().split('\n')

    # phase1(test)
    # phase1(data)
    phase2(test)
    phase2(data)


if __name__ == "__main__":
    main()
