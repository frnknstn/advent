import sys
import timeit
from typing import List, Tuple, Optional, Dict, Sequence, Iterable, Set, Generator, Callable
import collections
import re
from math import pow, sqrt, ceil, floor
from enum import Enum, IntEnum
import functools
import itertools

from a2023 import helper


Cell = collections.namedtuple('Cell', ("offset", "count"))


class CellSpec(IntEnum):
    OFF = 0
    ON = 1
    UNKNOWN = 3
    

class Pattern:
    def __init__(self, s: str):
        assert len(s) > 0
        pattern = []
        
        for c in s:
            if c == ".":
                pattern.append(CellSpec.OFF)
            elif c == "#":
                pattern.append(CellSpec.ON)
            elif c == "?":
                pattern.append(CellSpec.UNKNOWN)
                
        
        if pattern[0] != CellSpec.OFF:
            # all patterns start with an empty field
            pattern = [CellSpec.OFF] + pattern
        else:
            # collapse leading empties
            # not sure if this is needed or useful
            while len(pattern) > 1 and pattern[1] == CellSpec.OFF:
                pattern = pattern[1:]
                
        self.pattern = pattern
        self._s = s
    
    def is_match(self, cells: Sequence[Cell])->bool:
        """Return true if a supplied sequence can 'fit' in this pattern"""
        x:int = 0
        
        for offset, count in cells:
            for run in range(offset):
                if self.pattern[x + run] not in (CellSpec.OFF, CellSpec.UNKNOWN):
                    return False
            x += offset
            
            for run in range(count):  
                if self.pattern[x + run] not in (CellSpec.ON, CellSpec.UNKNOWN):
                    return False
            x += count
        
        # check for danging cells
        if x < len(self.pattern) and CellSpec.ON in self.pattern[x:]:
            return False

        return True
    
    def __len__(self):
        return len(self.pattern)
    
    def __repr__(self):
        return f"<Pattern '{self._s}'>"
            
                
def permutate_spec(spec: Sequence[int], length: int)->Iterable[List[Cell]]:
    """Permutate a spec and return all the possible Cell arrangements"""
    cell_count = len(spec)
    cell_width = sum(spec)
    freedom = length - cell_width
    max_gap = freedom - cell_count + 1
    
    for gaps in ([
        x for x in itertools.product(range(1, max_gap+1), repeat=cell_count) 
        if sum(x) <= freedom
    ]):
        yield [Cell(gap, sp) for gap, sp in zip(gaps, spec)]
        

def pretty_s(s):
    l = []
    for o, c in s:
        l.append('.'*o)
        l.append('#'*c)
    return "".join(l)


def phase1(data: List[str], quiet=False, **kwargs):
    if not quiet:
        print(f"# === Phase 1 with {len(data):d} lines")

    total = 0
    
    rows = []
    
    # parse input
    for line in data:
        pattern_str, spec_str = line.split()
        
        pattern = Pattern(pattern_str)
        spec = [int(x) for x in spec_str.split(',')]
        rows.append((pattern, spec, spec_str))
   
    for pattern, spec, spec_str in rows:
        wins = 0
        # print("-"*(2+len(pattern)), pattern)
        for s in permutate_spec(spec, len(pattern)):
            # print(pattern, pretty_s(s), spec, pattern.is_match(s))
            if pattern.is_match(s):
                wins += 1
        total += wins
            
    print("result", total)


def phase2(data: List[str], quiet=False, **kwargs):
    if not quiet:
        print(f"# === Phase 2 with {len(data):d} lines")
    total = 0
    

    print("result", total)


def main():
    data = helper.data_list(helper.guess_data_file(__file__))

    test = """
???.### 1,1,3
.??..??...?##. 1,1,3
?#?#?#?#?#?#?#? 1,3,1,6
????.#...#... 4,1,1
????.######..#####. 1,6,5
?###???????? 3,2,1
""".strip().split('\n')

    phase1(test)
    phase1(data)
    # phase2(test)
    # phase2(data)


if __name__ == "__main__":
    main()
