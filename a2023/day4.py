import timeit
from typing import List, Tuple, Optional, Dict, Sequence, Iterable, Set, Generator, Callable
import collections
import re
from math import pow

from a2023 import helper

data = None

def phase1(data: List[str], quiet=False, **kwargs):
    if not quiet:
        print(f"# === Phase 1 with {len(data):d} lines")
    
    total = 0
    
    for line in data:
        card_id, winning_str, my_str = re.fullmatch(r"Card[ ]+(\d+): ([0-9 ]+) \| ([0-9 ]+)", line).groups()
        winning_numbers, my_numbers = winning_str.split(), my_str.split()
        print(card_id, winning_numbers, my_numbers)
        
        wins = 0
        score = 0
        for needle in winning_numbers:
            if needle in my_numbers:
                wins += 1
        
        if wins > 0:
            #points = pow(2, wins - 1)
            points = 1 << (wins - 1)
            total += points
            
    print("total", total)
    
def phase2(data: List[str], quiet=False, **kwargs):
    if not quiet:
        print(f"# === Phase 2 with {len(data):d} lines")

    total = 0
    cards = [1] * len(data)

    for index, line in enumerate(data):
        card_id, winning_str, my_str = re.fullmatch(r"Card[ ]+(\d+): ([0-9 ]+) \| ([0-9 ]+)", line).groups()
        winning_numbers, my_numbers = winning_str.split(), my_str.split()
        print(card_id, winning_numbers, my_numbers)

        wins = 0
        for needle in winning_numbers:
            if needle in my_numbers:
                wins += 1

        while wins > 0:
            cards[index+wins] += cards[index]
            wins -= 1
    
    total = sum(cards)            

    print("total", total)


def main():
    global data
    data = helper.data_list(helper.guess_data_file(__file__))

    test = """Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11""".strip().split('\n')


    phase1(test)
    phase1(data)
    phase2(test)
    phase2(data)


if __name__ == "__main__":
    main()
