import timeit
from typing import List, Tuple, Optional, Dict
from functools import reduce
import re

from a2020 import helper

data = None


def parse_rules(data):
    what_can_hold = {}
    contents_lookup = {}

    # parse the rules
    for rule in data:
        match = re.match(r"^(.*) bags contain (.*)\.$", rule)
        if not match:
            print("Error matching rule line")
        outer_bag_color, bag_contents_str = match.groups()

        inner_bags = re.findall("(\d+) ([a-z ]+) bag", bag_contents_str)
        if not inner_bags:
            # contains no other bags
            pass

        # print(f"{outer_bag_color} -> {inner_bags}")

        # build the lookups
        bag_contents = []
        for count, color in inner_bags:
            count = int(count)
            d = what_can_hold.get(color, set())
            d.add(outer_bag_color)
            what_can_hold[color] = d
            
            bag_contents.extend([color] * count)
        
        contents_lookup[outer_bag_color] = bag_contents

    # parse the rules

    return what_can_hold, contents_lookup 


def phase1(what_can_hold):
    
    # trace the gold
    retval = set()
    to_check = list(what_can_hold["shiny gold"])
    checked = set()
    while to_check:
        color = to_check.pop()
        retval.add(color)
        checked.add(color)
        try:
            for c in what_can_hold[color]:
                if c not in checked:
                    to_check.append(c)
        except KeyError:
            pass
            # print("Nothing can hold " + c)
            
    return retval


def phase2(contents_lookup: Dict[str, List[str]]):
    
    contents = []
    to_check = contents_lookup["shiny gold"]
    
    while to_check:
        color = to_check.pop()
        contents.append(color)
        to_check.extend(contents_lookup[color])
    
    return contents


def main():
    global data
    data = helper.data_list("day7.txt")

    what_can_hold, contents_lookup = parse_rules(data)

    print(len(phase1(what_can_hold)))
    print(len(phase2(contents_lookup)))
    # print(sum(phase2(data)))

    # print("timed")
    print(timeit.timeit('a, b = parse_rules(data)', number=1000, globals=globals()))
    print(timeit.timeit('phase1(d)', setup='d, _ = parse_rules(data)', number=1000, globals=globals()))
    print(timeit.timeit('phase2(d)', setup='_, d = parse_rules(data)', number=1000, globals=globals()))
    # print(timeit.timeit('phase1(data, test_pw2)', number=1000, globals=globals()))


if __name__ == "__main__":
    main()
