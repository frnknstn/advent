import timeit
from typing import List, Tuple, Optional

from a2020 import helper

data = [ int(x) for x in helper.data_list("day1.txt") ]
target = 2020


def brute(data, target):
    """brute force soln"""
    data = data[:]
    
    while data:
        i = data.pop()
        for j in data:
            if i + j == target:
                return (i, j)
    return (None, None)


def brute_p2(data, target):
    """brute force soln"""
    orig_data = data[:]

    while orig_data:
        i = orig_data.pop()
        j, k = brute(orig_data, target - i)
        if j is not None:
            return (i, j, k)
        
    return (None, None, None)


def indexed(data: List[int], target: int) -> Tuple[Optional[int], Optional[int]]:
    """use a lookup index for improved big-O"""
    index = [False] * (target + 1)
    
    for i in data:
        # validate
        if i > target:
            print("data item is bigger than the target")
            continue
        elif i < 0:
            print("negative data is invalid")
            continue
        
        # do we have the needed value already?
        if index[target - i]:
            return (i, target - i)
        
        # record this value
        index[i] = True
    
    # no result
    return (None, None)


def brute_indexed_p2(data: List[int], target: int):
    """use the indexed inner"""
    orig_data = data[:]

    while orig_data:
        i = orig_data.pop()
        j, k = brute(orig_data, target - i)
        if j is not None:
            return (i, j, k)

    return (None, None, None)


def main():
    a, b = brute(data, target)
    print(a, b, a * b)

    a, b, c = brute_p2(data, target)
    print(a, b, c, a * b * c)

    a, b = indexed(data, target)
    print(a, b, a * b)

    a, b, c = brute_indexed_p2(data, target)
    print(a, b, c, a * b * c)

    print("timed")
    print(timeit.timeit('brute(data, target)', number=1000, globals=globals()))
    print(timeit.timeit('indexed(data, target)', number=1000, globals=globals()))
    print(timeit.timeit('brute_p2(data, target)', number=10, globals=globals()))
    print(timeit.timeit('brute_indexed_p2(data, target)', number=10, globals=globals()))


if __name__ == "__main__":
    main()
