import timeit
from typing import List, Tuple, Optional, Dict
from functools import reduce
import re

from a2020 import helper

data = None


def decode(instruction: str):
    op, param = instruction.split(" ", 1)
    return op, int(param)


def handhalt(data):
    """Run a program, check for halting. 
    Returns the value of the accumulator at boostrap completion.
    Returns None if the program does not terminate, or if there is a trapped access violation.
    """
    
    ip = 0
    ax = 0

    op_count = 0

    # when was this instruction first run?
    exec_record = [None] * len(data)

    while True:
        # check halting
        if exec_record[ip]:
            # found the loop!
            return None
        else:
            exec_record[ip] = op_count

        # decode and execute
        op, param = decode(data[ip])

        if op == "nop":
            ip += 1
        elif op == "acc":
            ax += param
            ip += 1
        elif op == "jmp":
            ip += param

        # check for exit
        if ip == len(data):
            # successful boot!
            return ax
        elif ip > len(data):
            print("access violation!")
            return None

        op_count += 1


def phase1(data):
    ip = 0
    ax = 0
    
    op_count = 0
    
    # when was this instruction first run?
    exec_record = [None] * len(data)
    
    while True:
        op, param = data[ip].split(" ", 1)
        param = int(param)
        
        if exec_record[ip]:
            # found the loop!
            return ax, exec_record[ip]
        else:
            exec_record[ip] = op_count

        if op == "nop":
            ip += 1
        elif op == "acc":
            ax += param
            ip += 1
        elif op == "jmp":
            ip += param
            
        op_count += 1


def phase2(data):
    for index, line in enumerate(data):
        op, param = decode(line)
        
        if op == "acc":
            continue
        elif op == "nop":
            mod_cmd = (index, "jmp " + str(param))
        elif op == "jmp":
            mod_cmd = (index, "nop " + str(param))
        else:
            raise NotImplementedError

        # run the modified code
        mod = data[:]
        mod[mod_cmd[0]] = mod_cmd[1]
        rc = handhalt(mod)
        if rc:
            return rc


def main():
    global data
    data = helper.data_list("day8.txt")

    print(phase1(data))
    print(phase2(data))


    # print("timed")
    # print(timeit.timeit('a, b = parse_rules(data)', number=1000, globals=globals()))
    print(timeit.timeit('phase1(data)', number=100, globals=globals()))
    print(timeit.timeit('phase2(data)', number=100, globals=globals()))
    # print(timeit.timeit('phase1(data, test_pw2)', number=1000, globals=globals()))


if __name__ == "__main__":
    main()
