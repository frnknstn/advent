import timeit
from typing import List, Tuple, Optional, Dict, Sequence, Iterable, Set, Generator
from functools import reduce
import itertools
import copy
import re
from time import sleep
import math

from a2020 import helper

data = None


def gen_zone_layer(zone: List[List[List]], size_dim: List[int]) -> Tuple[List[List[str]], Tuple[int]]:
    """generate all the 2-branes contained in a higher dimensional space"""
    excess_dims = size_dim[2:]
    excess_dims.reverse()
    
    for dim_spec in itertools.product(*tuple(range(i) for i in excess_dims)):
        target = zone
        for coord in dim_spec:
            target = target[coord]
        yield target, dim_spec


def print_conway(data: Set[Tuple], d: int = 3):
    # work out the size of arena
    min_dim = [0] * d
    max_dim = [0] * d

    for pos in data:
        for dim in range(d):
            this_dim = pos[dim]
            if this_dim < min_dim[dim]:
                min_dim[dim] = this_dim
            elif this_dim > max_dim[dim]:
                max_dim[dim] = this_dim

    size_dim = [0] * d

    for i in range(d):
        size_dim[i] = abs(min_dim[i]) + max_dim[i] + 1

    # prepare the draw zone
    zone = ['.'] * size_dim[0]
    for dim in size_dim[1:]:
        zone = [copy.deepcopy(zone) for _ in range(dim)]

        # plot the points
    for pos in data:
        # traverse the multi-dimensional array to find the end target 
        target = zone
        for i, coord in reversed(list(enumerate(pos))):
            if i != 0:
                # we need to go deeper
                target = target[coord - min_dim[i]]
            else:
                # plot the point
                target[coord] = '#'

    # draw the arena row by row
    key_line = []
    output_lines = []
    for y in range(size_dim[-2]):
        layer_lines = []
        for layer, coords in gen_zone_layer(zone, size_dim):
            # update our display key
            if coords not in key_line:
                key_line.append(coords)

            # draw the line of the layer
            row = layer[y]
            layer_line = "".join(row)
            layer_lines.append(layer_line)

        output_lines.append("  ".join(layer_lines))

    print("  ".join(str(x) for x in key_line))
    print("\n".join(output_lines))

    print()

    
def parse_data(data: List[str], d=3) -> Set[Tuple]:
    """return a set of X-dimensional points containing the supplied data on a 2brane"""
    dim_tail = (0,) * (d - 2) 
    retval = set()
    for y, row in enumerate(data):
        for x, c in enumerate(row):
            if c == '#':
                retval.add((x, y) + dim_tail)
    return retval


def gen_neighbours(pos: Tuple[int], d=3) -> Generator[Tuple[int], None, None]:
    """generator for all neighboring points in X-D space"""
    yield from (
        # add the elements from the supplied pos and our offset
        tuple(
            map(sum, zip(p, pos))
        ) 
        # generate all possible offsets
        for p in itertools.product((-1, 0, 1), repeat=d)
        # exclude our origin
        if p != (0,) * d
    )


def gen_neighbours_3d(pos: Tuple[int, int, int]) -> Generator[Tuple[int, int, int], None, None]:
    """generator for all neighboring points in 3D space"""
    neighbour_dir = (
        (-1, -1, -1), (0, -1, -1), (1, -1, -1),
        (-1,  0, -1), (0,  0, -1), (1,  0, -1),
        (-1,  1, -1), (0,  1, -1), (1,  1, -1),

        (-1, -1,  0), (0, -1,  0), (1, -1,  0),
        (-1,  0,  0),              (1,  0,  0),
        (-1,  1,  0), (0,  1,  0), (1,  1,  0),

        (-1, -1,  1), (0, -1,  1), (1, -1,  1),
        (-1,  0,  1), (0,  0,  1), (1,  0,  1),
        (-1,  1,  1), (0,  1,  1), (1,  1,  1),
    )
    # look in each dir
    for x_step, y_step, z_step in neighbour_dir:
        yield (pos[0] + x_step, pos[1] + y_step, pos[2] + z_step)


def conway_step(data: Set[Tuple], d: int=3) -> Set[Tuple]:
    """ Do one step of modified conway in infinite X-D space
    """

    # make the new ret val
    retval = data.copy()
    
    # track all points that can possibly be affected
    working_set = set()
    
    # process all active points
    for pos in data:
        neighbour_count = 0
        for neighbour in gen_neighbours(pos, d):
            if neighbour in data:
                neighbour_count += 1
                
            # remember this point for later
            working_set.add(neighbour)
        
        # check conway status
        if neighbour_count < 2 or neighbour_count > 3:
            # overcrowded
            retval.remove(pos)
    
    # remove active points from the working set
    working_set -= data
    
    # evaluate working set as inactive points
    for pos in working_set:
        neighbour_count = 0
        for neighbour in gen_neighbours(pos, d):
            if neighbour in data:
                neighbour_count += 1

        # check conway status
        if neighbour_count == 3:
            # new point
            retval.add(pos)

    return retval


def count_occupied(data: Set[Tuple]):
    return len(data)


def phase1(data: Set[Tuple], steps:int, d: int=3, quiet=False):
    if not quiet:
        print(f"# === Phase 1 with {len(data):d} lines")
    
    step = 0
    while True:
        pop = count_occupied(data)
        print("step =", step, " pop =", pop)
        
        if d >= 3:
            print_conway(data, d=d)

        # check if done
        if step >= steps:
            break
            
        # next step
        step += 1
        data = conway_step(data, d=d)


def phase2(data, needle: str = "departure", quiet: bool=False):
    if not quiet:
        print(f"# === Phase 1 with {len(data):d} lines")
           
    
def main():
    global data
    data = helper.data_list("day17.txt")

    test_data = """\
.#.
..#
###
""".strip().split('\n')
    

    print(phase1(parse_data(test_data[:]), steps=6, d=3))
    print(phase1(parse_data(data[:]), steps=6, d=3))
    print(phase1(parse_data(test_data[:], d=4), steps=6, d=4))
    print(phase1(parse_data(data[:], d=4), steps=6, d=4))

    # print("timed")
    # print(timeit.timeit('target = phase1(data)', number=100, globals=globals()))
    # print(timeit.timeit('phase1(data)', setup='target = phase1(data)', number=100, globals=globals()))
    # print(timeit.timeit('phase2(data, *target)', setup='target = phase1(data)', number=100, globals=globals()))


if __name__ == "__main__":
    main()
