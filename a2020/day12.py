import timeit
from typing import List, Tuple, Optional, Dict
from functools import reduce
import re
from time import sleep
import math

from a2020 import helper

data = None


class Ship:
    def __init__(self, x: int=0, y: int=0, heading: int=0):
        self.x = x
        self.y = y
        self._heading = heading  # east is 0 deg
    
    @property
    def pos(self):
        return (self.x, self.y)
    
    @property
    def heading(self):
        return self._heading
    
    @heading.setter
    def heading(self, val: int):
        # right angles only
        assert val % 90 == 0
        while val < 0:
            val += 360
        while val >= 360:
            val -= 360
        self._heading = val

    
    def nav(self, instruction):
        """enact one nav instruction"""
        cmd, val = instruction[0].upper(), int(instruction[1:])

        # convert "forward" command to the direction command
        if cmd =='F':
            heading = self.heading
            if heading == 0:
                cmd = 'E'
            elif heading == 90:
                cmd = 'S'
            elif heading == 180:
                cmd = 'W'
            elif heading == 270:
                cmd = 'N'
            else:
                raise RuntimeError("Bad ship heading angle detected")
        
        # handle the commands
        if cmd == 'N':
            self.y -= val
        elif cmd =='S':
            self.y += val
        elif cmd =='E':
            self.x += val
        elif cmd =='W':
            self.x -= val
        elif cmd =='L':
            self.heading -= val
        elif cmd =='R':
            self.heading += val


class Ship2:
    def __init__(self, x: int = 0, y: int = 0, heading: int = 0):
        """Just work in q1 :("""
        self.x = x
        self.y = y
        
        self.way_x_off = 10
        self.way_y_off = 1

    @property
    def pos(self):
        return (self.x, self.y)

    @property
    def waypoint(self):
        return(self.x + self.way_x_off, self.y + self.way_y_off)        

    def rotate_waypoint(self, val: int):
        rad = math.radians(val)
        x, y = self.way_x_off, self.way_y_off
        self.way_x_off = round((x * math.cos(rad)) - (y * math.sin(rad)))
        self.way_y_off = round((y * math.cos(rad)) + (x * math.sin(rad)))

    def nav(self, instruction):
        """enact one nav instruction"""
        cmd, val = instruction[0].upper(), int(instruction[1:])

        # handle the commands
        if cmd == 'F':
            for _ in range(val):
                self.x, self.y = self.waypoint
        elif cmd == 'N':
            self.way_y_off += val
        elif cmd == 'S':
            self.way_y_off -= val
        elif cmd == 'E':
            self.way_x_off += val
        elif cmd == 'W':
            self.way_x_off -= val
        elif cmd == 'L':
            self.rotate_waypoint(val)
        elif cmd == 'R':
            self.rotate_waypoint(-val)

def phase1(data):
    ship = Ship()
    
    for instruction in data:
        ship.nav(instruction)
        
    return ship.pos, helper.manhattan(ship.pos)


def phase2(data):
    ship = Ship2()

    for instruction in data:
        ship.nav(instruction)

    return ship.pos, helper.manhattan(ship.pos)


def main():
    global data
    data = helper.data_list("day12.txt")

    test_data = """\
F10
N3
F7
R90
F11""".strip().split()

    # print(phase1(test_data[:]))
    # print(phase1(data[:]))
    print(phase2(test_data[:]))
    print(phase2(data[:]))

    # print("timed")
    # print(timeit.timeit('target = phase1(data)', number=100, globals=globals()))
    # print(timeit.timeit('phase1(data)', setup='target = phase1(data)', number=100, globals=globals()))
    # print(timeit.timeit('phase2(data, *target)', setup='target = phase1(data)', number=100, globals=globals()))


if __name__ == "__main__":
    main()
