import timeit
from typing import List, Tuple, Optional, Dict, Sequence, Iterable, Set, Generator, Callable
from functools import reduce
import itertools
import copy
import re
from time import sleep
import math

from a2020 import helper

data = None


def parse_data(data: List[str]) -> List[Tuple[str]]:
    """tokenise the data
    
    "7 + 3 + (5 + (7 + 2 * 8 + 3)) + (4 * 5 + (7 + 2) + 2)"
     -> 
     (['7', '+', '3', '+', '(', '5', '+', '(', '7', '+', '2', '*', '8', '+', '3', ')', ')', '+', '(', '4', '*', '5', '+', '(', '7', '+', '2', ')', '+', '2', ')')
    """
    regex = re.compile(r"\s|([()])")
    
    return [tuple(y for y in re.split(regex, x.strip()) if y) for x in data]


def calc_part(eq: Sequence[str], start) -> Tuple[int, int]:
    """calculate an equation segment recursively from a starting point until the end or closing bracket
    
    Returns the result of the segment, and the index of the last token consumed
    """
    i = start
    state = "lhs"  # ["lhs", "op", '+', '*']
    
    result = 0
    
    while True:
        token = eq[i]
        
        if token == ')':
            # we are done
            break
        elif state == "op":
            # we are expecting an operation
            if token == '+':
                state = '+'
            elif token == '*':
                state = '*'
        else:
            # we are expecting a value
            if token == '(':
                # recurse
                val, i = calc_part(eq, i + 1)
            else:
                val = int(token)
            
            # do something with the value
            if state == "lhs":
                result = val
                state = "op"
            elif state == '+':
                result += val
                state = "op"
            elif state == '*':
                result *= val
                state = "op"
        
        # are we at the end of the expression?
        i += 1
        if i >= len(eq):
            break
    
    return result, i
    
    
def prepare_rpn(eq: Sequence[str]) -> Tuple[int, int]:
    """Express the calculation in reverse polish notation using Dykstra's shunting yard algorithm"""
    
    precedence = {"*":0, "+":1}
    operators = "*+"
    
    out_queue = []
    op_stack = []
    
    digit_re = re.compile(r"^\d+$")
    
    for token in eq:
        
        if digit_re.match(token):
            # handle a value
            out_queue.append(int(token))
        else:
            if token == "(":
                op_stack.append(token)
            elif token in operators:
                # check pending operations
                while op_stack and op_stack[-1] != "(":
                    if precedence[token] > precedence[op_stack[-1]]:
                        break
                    else:
                        out_queue.append(op_stack.pop())
                op_stack.append(token)
                
            elif token == ")":
                # flush pending operations
                while op_stack[-1] != "(":
                    out_queue.append(op_stack.pop())
                # discard the bracket
                op_stack.pop()
            else:
                raise Exception("Error with RPN conversion")
                        
    # flush pending operations
    while op_stack:
        out_queue.append(op_stack.pop())
                        
    return out_queue
    
                
def process_rpn(rpn: Iterable[int]) -> int:
    """calculate a reverse polish equation"""
    stack = []
    
    for token in rpn:
        if isinstance(token, int):
            stack.append(token)
        elif token == "+":
            lhs = stack.pop()
            rhs = stack.pop()
            stack.append(rhs + lhs)
        elif token == "*":
            lhs = stack.pop()
            rhs = stack.pop()
            stack.append(lhs * rhs)
    
    assert(len(stack) == 1)
    return stack[0]
            
    
                
def calc_part_rpn(eq: Sequence[str], start) -> Tuple[int, int]:   
    # TODO: This
    rpn = prepare_rpn(eq)
    result = process_rpn(rpn)
    return result, len(eq)

    

def phase1(data: List[str], calc_func: Callable=calc_part, quiet=False):
    if not quiet:
        print(f"# === Phase 1 with {len(data):d} lines")
    
    # tokenise and parse
    data = parse_data(data)
    
    total = 0
    for line in data:
        result, _ = calc_func(line, start=0)
        total += result
        print("".join(line), "=", result)
    
    print("total is", total)
    return total


def main():
    global data
    data = helper.data_list("day18.txt")

    test_data = """\
1 + 2 * 3 + 4 * 5 + 6
1 + (2 * 3) + (4 * (5 + 6))
2 * 3 + (4 * 5)
5 + (8 * 3 + 9 + 3 * 4 * 3)
5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))
((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2
""".strip().split('\n')
    

    # print(phase1(test_data[:]))
    # print(phase1(data[:]))
    print(phase1(test_data[:], calc_func=calc_part_rpn))
    print(phase1(data[:], calc_func=calc_part_rpn))


    # print(phase1(parse_data(test_data[:], d=4), steps=6, d=4))
    # print(phase1(parse_data(data[:], d=4), steps=6, d=4))

    # print("timed")
    # print(timeit.timeit('target = phase1(data)', number=100, globals=globals()))
    # print(timeit.timeit('phase1(data)', setup='target = phase1(data)', number=100, globals=globals()))
    # print(timeit.timeit('phase2(data, *target)', setup='target = phase1(data)', number=100, globals=globals()))


if __name__ == "__main__":
    main()
