import timeit
from typing import List, Tuple, Optional, Dict
from functools import reduce
import re

from a2020 import helper

data = None


def phase1(data):
    data.sort()
    
    gaps = []
    
    last = 0
    for i in data:
        gaps.append(i - last)
        last = i
    
    # add the built-in
    last = last + 3
    gaps.append(3)
    
    assert gaps.count(2) == 0
    
    return [ gaps.count(x) for x in range(1, 4) ], last


prog = 0
prog_max = 0
prog_up = True
def progress(val):
    """estimate the progress of a recursive function"""
    global prog, prog_up, prog_max
    if not prog_up:
        # progress phase
        if val < prog:
            prog = val
            print(val, prog_max, (1- (prog / prog_max)) * 100)
    else:
        # still preparing
        if val > prog:
            prog = val
        else:
            prog_max = prog
            prog_up = False


def p2_recurse(sorted_data, start, last):
    count = 0
    max_item = start + 3
    
    for index, item in enumerate(sorted_data):
        if item > max_item:
            # run out of valid items
            return count
        elif item == last:
            # base case, return what we have
            return count + 1
        else:
            if index == len(sorted_data) - 3:
                # base base case, only two items means only one path
                count += 1
            else:
                # recurse
                sub_data = sorted_data[index+1:]
                count += p2_recurse(sub_data, item, last)
                progress(item)


def phase2(data, last):
    """This will work, but may take years"""
    data.sort()
    data.append(last)
    return p2_recurse(data, 0, last)


def phase2_split(data):
    """If there are gaps of 3 between values, we can treat each as a sub-problem"""
    
    # work out where the hard gaps are
    data.sort()
    data = [0] + data

    subsets = []

    last_val = None
    run_start = 0
    for index, val in enumerate(data):
        if last_val is None:
            last_val = val
            continue
        diff = val - last_val
        if diff == 3:
            subsets.append(data[run_start:index])
            run_start = index
        last_val = val
    if run_start != index:
        subsets.append(data[run_start:])
    
    # calc the prod
    total = 1
    for run in subsets:
        print(run)
        assert len(run) <= 5
        
        # base cases:
        sz = len(run)
        if sz <= 2:
            c = 1
        elif sz == 3:
            c = 2
        elif sz == 4:
            c = 4
        elif sz == 5:
            c = 7
        else:
            assert False
        print(c)
        total *= c
        
    print("total: " + str(total))
    
    return subsets
    


def main():
    global data
    data = [int(x) for x in helper.data_list("day10.txt")]

    # data = [16, 10, 15, 5, 1, 11, 7, 19, 6, 12, 4]
    # data = [28, 33, 18, 42, 31, 14, 46, 20, 48, 47, 24, 23, 49, 45, 19, 38, 39, 11, 1, 32, 25, 35, 8, 17, 7, 9, 4, 2, 34, 10, 3]
    
    
    print("0 " + str(sorted(data)) + " " + str(max(data) + 3))
    
    print(phase1(data[:]))
    # print(phase2(data[:], phase1(data[:])[-1]))
    print(phase2_split(data[:]))

    # print("timed")
    # print(timeit.timeit('target = phase1(data)', number=100, globals=globals()))
    # print(timeit.timeit('phase1(data)', setup='target = phase1(data)', number=100, globals=globals()))
    # print(timeit.timeit('phase2(data, *target)', setup='target = phase1(data)', number=100, globals=globals()))


if __name__ == "__main__":
    main()
