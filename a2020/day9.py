import timeit
from typing import List, Tuple, Optional, Dict
from functools import reduce
import re

from a2020 import helper

data = None


class CircularBuffer:
    def __init__(self, size: int, default=None):
        self.data = [default]*size
        self.index = 0
        self.size = size
    
    def add(self, v):
        self.data[self.index] = v
        self.index += 1
        while self.index >= self.size:
            self.index -= self.size


def phase1(data):
    # initialise the buffer using the preamble
    buf = CircularBuffer(25)
    for index in range(25):
        buf.add(data[index])
        
    # start of real data
    index = 25
    while True:
        val = data[index]
        
        # brute force it
        found = False
        for fa in buf.data:
            target = val - fa
            if target == fa:
                # can't have doubles
                continue
            if target in buf.data:
                found = True
                break

        if not found:
            return val, index
        
        buf.add(val)
        index += 1
         

def phase2(data, target, target_index):
    for start in range(target_index):
        total = 0
        i = 0
        min_v = target
        max_v = 0
        
        while total < target:
            new = data[start + i]
            total += new
            min_v = min(min_v, new)
            max_v = max(max_v, new)
            
            i += 1
        
        if total == target:
            return(min_v, max_v)


def min_and_max(data, i, j):
    run = data[i:j+1]
    return(min(run), max(run))


def phase2_partial(data, target, target_index):
    # build list of partial sums
    partials = []
    total = 0
    start, end = 0, target_index
    for i in range(target_index):
        total += data[i]
        
        # did we lucksack?
        if total == target:
            start = 0
            end = i
            break
        
        partials.append(total)
    
    if total == target:
        return min_and_max(data, start, end)
    
    # do the calc
    for start in range(1, target_index):
        for end in range(start + 1, target_index):
            total = partials[end] - partials[start - 1]
            if total > target:
                break
            elif total == target:
                return min_and_max(data, start, end)


def phase2_walk(data, target, target_index):
    start = 0
    end = 1
    total = data[0] + data[1]

    while True:
        if total < target:
            end += 1
            total += data[end]
        elif total > target:
            total -= data[start]
            start += 1
        else:
            # found it
            return min_and_max(data, start, end)
        

def main():
    global data
    data = [int(x) for x in helper.data_list("day9.txt")]

    print(phase1(data))
    print(sum(phase2(data, *phase1(data))))
    print(sum(phase2_partial(data, *phase1(data))))
    print(sum(phase2_walk(data, *phase1(data))))

    # print("timed")
    print(timeit.timeit('target = phase1(data)', number=100, globals=globals()))
    print(timeit.timeit('phase1(data)', setup='target = phase1(data)', number=100, globals=globals()))
    print(timeit.timeit('phase2(data, *target)', setup='target = phase1(data)', number=100, globals=globals()))
    print(timeit.timeit('phase2_partial(data, *target)', setup='target = phase1(data)', number=100, globals=globals()))
    print(timeit.timeit('phase2_walk(data, *target)', setup='target = phase1(data)', number=100, globals=globals()))


if __name__ == "__main__":
    main()
