import timeit
from typing import List, Tuple, Optional, Dict, Sequence, Iterable, Set
from functools import reduce
import re
from time import sleep
import math

from a2020 import helper

data = None


def parse_data(data: List[str]) -> Tuple[List[Tuple[str, Tuple[int, int], Tuple[int, int]]], List[int], List[List[int]]]:
    """parse a data file into rules, your ticket, and the other tickets"""
    rules = []
    my_ticket = []
    other_tickets = []
    
    rule_re = re.compile(r"^([\w\s]+):\s*([0-9]+)-([0-9]+) or ([0-9]+)-([0-9]+)$")
    
    state = 0
    for line in data:
        line = line.strip()
        if not line:
            state += 1
            continue
        
        if state == 0:
            # rules
            nr = rule_re.match(line).groups()
            rules.append((
                nr[0], 
                (int(nr[1]), int(nr[2])),
                (int(nr[3]), int(nr[4])),
            ))
        elif state == 1:
            if "," in line: 
                my_ticket = [int(x) for x in line.split(',')]
        elif state >= 2:
            if "," in line:
                other_tickets.append([int(x) for x in line.split(',')])
    
    return (rules, my_ticket, other_tickets)


def phase1(data: Tuple[List[Tuple[str, Tuple[int, int], Tuple[int, int]]], List[int], List[List[int]]], quiet=False):
    if not quiet:
        print(f"# === Phase 1 with {len(data):d} lines")
    
    rules, my_ticket, other_tickets = data
    
    # ticket scanning error rate
    tser = 0
    
    for ticket in other_tickets:
        for field in ticket:
            valid = False
            for rule_name, rule_a, rule_b in rules:
                if (rule_a[0] <= field <= rule_a[1]) or (rule_b[0] <= field <= rule_b[1]):
                    valid = True
                    continue
            if not valid:
                tser += field
    
    return tser 


def phase2(data, needle: str = "departure"):
    print(f"# === Phase 2 with {len(data):d} lines")

    rules, my_ticket, other_tickets = data
    
    # exclude obvious bad tickets
    print(len(other_tickets))
    other_tickets = [x for x in other_tickets if phase1((rules, [], [x]), quiet=True) == 0]
    print(len(other_tickets))
    
    field_count = len(my_ticket)
    rule_breaks = {key: set() for key in [x[0] for x in rules]}
    
    # work out the impossible values
    for ticket in other_tickets:
        for field_index, val in enumerate(ticket):
            for rule_name, rule_a, rule_b in rules:
                if (rule_a[0] <= val <= rule_a[1]) or (rule_b[0] <= val <= rule_b[1]):
                    # valid
                    pass
                else:
                    # mark the violation
                    breakers = rule_breaks[rule_name]
                    if field_index not in breakers:
                        print(f"'{rule_name}' can't be field {field_index:d}")
                    breakers.add(field_index)
    
    # collapse the impossible values until we have narrowed down the rules
    
    # start with the rule that is broken by the most fields
    sorted_breaks = sorted(rule_breaks.items(), key=lambda x: (len(x[1]), x[0]), reverse=True)
    for key, value in sorted_breaks:
        print(len(value), key, value)

    # a list of fields that don't have a rule yet
    all_field_candidates = list(range(len(my_ticket)))
    confirmed_fields = {}
    
    for rule_name, rule_breakers in sorted_breaks:
        # starting from the list of fields that don't have a rule yet, remove all the fields that break our rule
        field_candidates = all_field_candidates[:]
        for field_index in rule_breakers:
            field_candidates.remove(field_index)
        
        # I am assuming that the puzzle input gives only one valid solution
        if len(field_candidates) != 1:
            print(f"Error on rule {rule_name:s}")
            continue

        field_index = field_candidates[0]
        print(f"'{rule_name:s}' is field {field_index:d}")
        confirmed_fields[rule_name] = field_index
        all_field_candidates.remove(field_index)
    
    # work out the actual thing the question was asking for
    product = 1
    for key, value in confirmed_fields.items():
        if key.startswith(needle):
            product *= my_ticket[value]
    return product
    
    
def main():
    global data
    data = helper.data_list("day16.txt")

    test_data = """\
class: 1-3 or 5-7
row: 6-11 or 33-44
seat: 13-40 or 45-50

your ticket:
7,1,14

nearby tickets:
7,3,47
40,4,50
55,2,20
38,6,12
""".strip().split('\n')

    test_data_2 = """\
class: 0-1 or 4-19
row: 0-5 or 8-19
seat: 0-13 or 16-19

your ticket:
11,12,13

nearby tickets:
3,9,18
15,1,5
5,14,9
""".strip().split('\n')
    

    print(phase1(parse_data(test_data[:])))
    print(phase1(parse_data(data[:])))
    # print(phase2(data[:]))
    print(phase2(parse_data(test_data_2[:]), "row"))
    print(phase2(parse_data(data[:])))

    # print("timed")
    # print(timeit.timeit('target = phase1(data)', number=100, globals=globals()))
    # print(timeit.timeit('phase1(data)', setup='target = phase1(data)', number=100, globals=globals()))
    # print(timeit.timeit('phase2(data, *target)', setup='target = phase1(data)', number=100, globals=globals()))


if __name__ == "__main__":
    main()
