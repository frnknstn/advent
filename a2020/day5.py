import timeit
from typing import List, Tuple, Optional
from functools import reduce
import re

from a2020 import helper

data = None


def seat_id(s):
    """these are just binary values"""
    b = 9
    x = 0
    for c in s:
        if c in "BR":
            x += 1 << b
        b -= 1
    return x


def phase1(data):
    return max(seat_id(x) for x in data)


def phase2(data):
    s = set(seat_id(x) for x in data)
    for i in range(1024):
        if i not in s and i + 1 in s and i - 1 in s:
            return i    


def main():
    global data
    data = helper.data_list("day5.txt")

    print(phase1(data))
    print(phase2(data))

    # phase2_counts = []
    # for x, y in [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]:
    #     phase2_counts.append(phase1(data, x, y))
    # print(phase2_counts, reduce(lambda x, y: x * y, phase2_counts))

    # print("timed")
    # print(timeit.timeit('phase1(data, test_pw1)', number=1000, globals=globals()))
    # print(timeit.timeit('phase1(data, test_pw2)', number=1000, globals=globals()))


if __name__ == "__main__":
    main()
