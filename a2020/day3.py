import timeit
from typing import List, Tuple, Optional
from functools import reduce

from a2020 import helper
data = None


def phase1(data, x_step: int, y_step: int):
    x, y = 0, 0
    rows = len(data)
    cols = len(data[0])
    
    trees = 0
    
    while y < rows:
        if data[y][x] == '#':
            trees += 1

        x += x_step
        y += y_step

        # wrap at the right
        while x >= cols:
            x -= cols
    
    return trees
        

def main():
    global data
    data = [tuple(x) for x in helper.data_list("day3.txt")]

    print(phase1(data, 3, 1))
    
    phase2_counts = []
    for x, y in [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]:
        phase2_counts.append(phase1(data, x, y))
    print(phase2_counts, reduce(lambda x, y: x * y, phase2_counts))

    # print("timed")
    # print(timeit.timeit('phase1(data, test_pw1)', number=1000, globals=globals()))
    # print(timeit.timeit('phase1(data, test_pw2)', number=1000, globals=globals()))


if __name__ == "__main__":
    main()
