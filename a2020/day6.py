import timeit
from typing import List, Tuple, Optional
from functools import reduce
import re

from a2020 import helper

data = None


def phase1(data):
    results = []
    
    answers = []
    
    for person in data:
        person = person.strip()
        if not person:
            # empty line, end of a gang
            if answers:
                results.append(answers.count(True))
            answers = [False] * 26
            continue
        for c in person:
            try:
                answers[ord(c) - 97] = True
            except IndexError:
                print(c, ord(c), ord(c) - 97)
    
    return results


def phase2(data):
    results = []

    common_answers = set()
    answers = set()
    
    for person in data:
        person = person.strip()

        if not person:
            # empty line, end of a gang
            if common_answers:
                results.append(len(common_answers))
            new_gang = True
            continue
            
        answers = set(person)
        
        if new_gang:
            # use the first member's answers as the base for the set
            common_answers = answers
            new_gang = False
        else:
            # only use the overlapping answers
            common_answers = common_answers.intersection(answers)

    return results

def main():
    global data
    data = helper.data_list("day6.txt", strip=False)

    print(sum(phase1(data)))
    print(sum(phase2(data)))


    # print("timed")
    # print(timeit.timeit('phase1(data, test_pw1)', number=1000, globals=globals()))
    # print(timeit.timeit('phase1(data, test_pw2)', number=1000, globals=globals()))


if __name__ == "__main__":
    main()
