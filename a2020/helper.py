from typing import Tuple, List 
import os
import math

module = "a2020"


def data_raw(filename):
    with open(os.path.join(module, filename), "rt") as fp:
        return fp.read()


def data_list(filename, strip=True) -> List:
    with open(os.path.join(module, filename), "rt") as fp:
        data = fp.read()
        if strip:
            data = data.strip()
        return data.split("\n")

def manhattan(pos: Tuple[int, int]):
    """returns the Manhattan distance from origin"""
    return abs(pos[0]) + abs(pos[1])


def lcm(a: int, b: int) -> int:
    """returns the lowest common multiple of two numbers"""
    return abs(a * b) // int(math.gcd(a, b))

