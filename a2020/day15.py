import timeit
from typing import List, Tuple, Optional, Dict, Sequence, Iterable
from functools import reduce
import re
from time import sleep
import math

from a2020 import helper

data = None


def phase1(data: List[str], target: int=2020):
    print(f"# === Phase 1 with {len(data):d} lines")
    print(f"# doing {target:d} loops")
    
    assert len(data) > 0
    assert len(data) <= target
    
    data = [int(x) for x in data]

    prev = {}
    last = {}
    spoken = []
    count = 0

    def speak(val: int):
        nonlocal prev, last, count, spoken
        # print(val)
        count += 1
        spoken.append(val)
        if val in last:
            # we have seen this before
            prev[val] = last[val]
        last[val] = count

    # replay the historic data
    for val in data[:-1]:
        speak(val)
    
    # prime the game
    val = data[-1]

    # play the game to completion
    while count < target:
        speak(val)
        
        val = count - prev.get(val, count)
    
    return spoken[-1]


def phase2(data: List[str], target: int=30000000):
    # Phase 2 runs a bit slow but I don't really feel like optimising it. I know there is a loop in the data 
    # somewhere, but my run time and memory usage with the 2x dicts are acceptable.
    return phase1(data, target)
    
    
def main():
    global data
    data = helper.data_raw("day15.txt").split(",")

    test_data = """\
0,3,6
""".strip().split(',')
    

    print(phase1(test_data[:], 10))
    print(phase1(data[:]))
    print(phase2(data[:]))
    # print(phase2(test_data_2[:]))
    # print(phase2(data[:]))

    # print("timed")
    # print(timeit.timeit('target = phase1(data)', number=100, globals=globals()))
    # print(timeit.timeit('phase1(data)', setup='target = phase1(data)', number=100, globals=globals()))
    # print(timeit.timeit('phase2(data, *target)', setup='target = phase1(data)', number=100, globals=globals()))


if __name__ == "__main__":
    main()
