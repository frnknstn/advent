import timeit
from typing import List, Tuple, Optional
from functools import reduce
import re

from a2020 import helper

data = None


def phase1(data):
    valid_count = 0
    for doc in data:
        field_count = 0
        for field in doc.split():
            head, tail = field.split(":")
            if head == "byr": # (Birth Year)
                field_count += 1
            elif head == "iyr": # (Issue Year)
                field_count += 1
            elif head == "eyr": # (Expiration Year)
                field_count += 1
            elif head == "hgt": # (Height)
                field_count += 1
            elif head == "hcl": # (Hair Color)
                field_count += 1
            elif head == "ecl": # (Eye Color)
                field_count += 1
            elif head == "pid": # (Passport ID)
                field_count += 1
            elif head == "cid": # (Country ID)
                # passport
                pass
            else: # wierd field
                pass
        if field_count >= 7:
            valid_count += 1
    return valid_count


def phase2(data):
    valid_count = 0
    for doc in data:
        field_count = 0
        for field in doc.split():
            head, tail = field.split(":")
            if head == "byr": # (Birth Year)
                if 1920 <= int(tail) <= 2002:
                    field_count += 1
            elif head == "iyr": # (Issue Year)
                if 2010 <= int(tail) <= 2020:
                    field_count += 1
            elif head == "eyr": # (Expiration Year)
                if 2020 <= int(tail) <= 2030:
                    field_count += 1
            elif head == "hgt": # (Height)
                match = re.match(r"^([0-9]+)(cm|in)$", tail)
                if match:
                    height, unit = match.groups()
                    height = int(height)
                    if unit == "cm" and 150 <= height <= 193:
                        field_count += 1
                    elif unit == "in" and 59 <= height <= 76:
                        field_count += 1
            elif head == "hcl": # (Hair Color)
                if re.match(r"^#[0-9a-f]{6}$", tail):
                    field_count += 1
            elif head == "ecl": # (Eye Color)
                if tail in ("amb", "blu", "brn", "gry", "grn", "hzl", "oth"):
                    field_count += 1
            elif head == "pid": # (Passport ID)
                if re.match(r"^[0-9]{9}$", tail):
                    field_count += 1
            elif head == "cid": # (Country ID)
                # passport
                pass
            else: # wierd field
                pass
        if field_count >= 7:
            valid_count += 1
    return valid_count


def main():
    global data
    data = helper.data_raw("day4.txt")
    
    data = data.split("\n\n")
    
    print(phase1(data))
    print(phase2(data))
    
    
    # phase2_counts = []
    # for x, y in [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]:
    #     phase2_counts.append(phase1(data, x, y))
    # print(phase2_counts, reduce(lambda x, y: x * y, phase2_counts))

    # print("timed")
    # print(timeit.timeit('phase1(data, test_pw1)', number=1000, globals=globals()))
    # print(timeit.timeit('phase1(data, test_pw2)', number=1000, globals=globals()))


if __name__ == "__main__":
    main()
