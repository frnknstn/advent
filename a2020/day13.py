import timeit
from typing import List, Tuple, Optional, Dict
from functools import reduce
import re
from time import sleep
import math

from a2020 import helper

data = None

def phase1(data):
    # Solved by inspection using a spreadsheet
    return 3246


def phase2(data, start: int = 100000000000000):
    data = data[1].strip().split(",")

    # From the problem:
    # However, with so many bus IDs in your list, surely the actual earliest timestamp will be larger than 100000000000000!
    timestamp = start
    
    # work out the offsets for each bus ID
    offsets = [ (i, int(bus.strip())) for i, bus in enumerate(data) if bus.strip() != 'x' ]
    stride = 1

    for index, (offset, bus) in enumerate(offsets):
        # for each bus, step through until we find an entry that satisfies our offset requirement
        step_count = 0
        while (timestamp + offset) % bus != 0: 
            timestamp += stride
            step_count += 1
        
        print(f"found bus {index} ({bus}) at offset {offset} from timestamp {timestamp} via stride {stride} after {step_count} steps")
        stride = helper.lcm(stride, bus)
    
    return timestamp


def main():
    global data
    data = helper.data_list("day13.txt")

    test_data = """\
939
7,13,x,x,59,x,31,19""".strip().split('\n')
    
    print(phase1(data[:]))
    print(phase2(test_data[:], start=0))
    print(phase2(data[:]))

    # print("timed")
    # print(timeit.timeit('target = phase1(data)', number=100, globals=globals()))
    # print(timeit.timeit('phase1(data)', setup='target = phase1(data)', number=100, globals=globals()))
    # print(timeit.timeit('phase2(data, *target)', setup='target = phase1(data)', number=100, globals=globals()))


if __name__ == "__main__":
    main()
