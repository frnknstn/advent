import timeit
from typing import List, Tuple, Optional

from a2020 import helper


def test_pw1(low: int, high:int, target:str, pw:str) -> bool:
    count = 0
    for char in pw:
        if char == target:
            count += 1
            if count > high:
                return False
    
    if count < low:
        return False
    return True


def test_pw2(low: int, high: int, target: str, pw: str) -> bool:
    count = 0
    # XOR
    return (pw[low - 1] == target) != (pw[high - 1] == target)


def phase1(data, test_func):
    valid_count = 0
    for row in data:
        # example data:
        # 5-10 b: bhbjlkbbbbbbb
        low, high = [ int(x) for x in row[0].split('-', 1) ]
        target = row[1][0]
        pw = row[2]
        
        if test_func(low, high, target, pw):
            valid_count += 1
    
    return valid_count


data = None

def main():
    global data
    data = [x.split() for x in helper.data_list("day2.txt")]
    
    print(phase1(data, test_pw1))
    print(phase1(data, test_pw2))

    print("timed")
    print(timeit.timeit('phase1(data, test_pw1)', number=1000, globals=globals()))
    print(timeit.timeit('phase1(data, test_pw2)', number=1000, globals=globals()))


if __name__ == "__main__":
    main()
