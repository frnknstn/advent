import timeit
from typing import List, Tuple, Optional, Dict
from functools import reduce
import re
from time import sleep
from copy import deepcopy


from a2020 import helper

data = None


def count_neighbours(data: List[List[str]], x, y) -> int:
    """Returns the number of adjacent chairs that are occupied"""
    def is_occupied(data, x, y, width, height):
        """returns false if a chair does not exist or is empty"""
        if x < 0 or x >= width or y < 0 or y >= height:
            # not a valid seat location
            return False
        return (data[y][x] == '#')
    
    height, width = len(data), len(data[0])
    neighbour_chairs = (
        (x-1, y-1), (x, y-1), (x+1, y-1),
        (x-1, y  ),           (x+1, y  ),
        (x-1, y+1), (x, y+1), (x+1, y+1),
    )
    
    return sum(is_occupied(data, a, b, width, height) for a, b in neighbour_chairs)


def count_neighbours_p2(data: List[List[str]], x, y) -> int:
    """Returns the number of nearest chairs in semi-cardinal directions that are occupied"""
    
    def is_oob(data, x, y, width, height):
        if x < 0 or x >= width or y < 0 or y >= height:
            # not a valid seat location
            return True
        return False

    height, width = len(data), len(data[0])
    neighbour_dir = (
        (-1, -1), (0, -1), (1, -1),
        (-1,  0),          (1,  0),
        (-1,  1), (0,  1), (1,  1),
    )
    
    total = 0
    # look in each dir
    for x_step, y_step in neighbour_dir:
        lx, ly = x, y
        while True:
            lx, ly = lx + x_step, ly + y_step
            if is_oob(data, lx, ly, width, height):
                break
            if data[ly][lx] == '.':
                # skip empty floor
                continue
            else:
                # found a chair
                if data[ly][lx] == '#':
                    total += 1
                break

    return total


def conway_step(data: List[List[str]], neighbour_func=count_neighbours, threshold: int = 4) -> List[List[str]]:
    """ Do one step of modified conway
    
    * If a seat is empty (L) and there are no occupied seats adjacent to it, the seat becomes occupied.
    * If a seat is occupied (#) and four or more seats adjacent to it are also occupied, the seat becomes empty.
    * Otherwise, the seat's state does not change.

    Floor (.) never changes; seats don't move, and nobody sits on the floor.
    """
    
    # make the new ret val
    chairs = deepcopy(data)
    
    for y, row in enumerate(data):
        for x, c in enumerate(row):
            if c == '.':
                # floor
                continue
                
            neighbour_count = neighbour_func(data, x, y)
            if c == 'L' and neighbour_count == 0:
                # fill empty seat
                chairs[y][x] = '#'
            elif c == '#' and neighbour_count >= threshold:
                # empty crowded chair
                chairs[y][x] = 'L'
            
    return chairs 
    

def count_occupied(data: List[List[str]]):
    occupied = 0
    for row in data:
        occupied += sum((c == '#') for c in row)
    return occupied


def print_conway(data: List[List[str]]):
    for row in data:
        print("".join(row))


def phase1(data):
    step = 0
    last_ppl = None

    while True:
        print_conway(data)
        ppl = count_occupied(data)
        print(step, ppl)
        if ppl == last_ppl:
            return
        last_ppl = ppl
        
        # next step
        step += 1
        data = conway_step(data)


def phase2(data):
    step = 0
    last_ppl = None

    while True:
        print_conway(data)
        ppl = count_occupied(data)
        print(step, ppl)
        if ppl == last_ppl:
            return
        last_ppl = ppl

        # next step
        step += 1
        data = conway_step(data, neighbour_func=count_neighbours_p2, threshold=5)


def main():
    global data
    data = [list(x) for x in helper.data_list("day11.txt")]
    
    test_data = [list(x) for x in """\
L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL""".strip().split()]
    
    # print(phase1(test_data[:]))
    # print(phase1(data[:]))
    print(phase2(test_data[:]))
    print(phase2(data[:]))

    # print("timed")
    # print(timeit.timeit('target = phase1(data)', number=100, globals=globals()))
    # print(timeit.timeit('phase1(data)', setup='target = phase1(data)', number=100, globals=globals()))
    # print(timeit.timeit('phase2(data, *target)', setup='target = phase1(data)', number=100, globals=globals()))


if __name__ == "__main__":
    main()
