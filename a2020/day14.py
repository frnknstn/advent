import timeit
from typing import List, Tuple, Optional, Dict, Sequence, Iterable
from functools import reduce
import re
from time import sleep
import math

from a2020 import helper

data = None


def checksum(mem: Iterable[int]) -> int:
    """Checksum of the soln memory required by the task.
    
    Actually just the sum of the memory"""
    return sum(mem)


def apply_mask(mask: str, p: int):
    """Apply a bitmask to an integer"""
    mask = mask.upper()
    for i, c in enumerate(reversed(mask)):
        if c == 'X':
            continue
        elif c == '0':
            p = p & (~ (2 ** i))
        elif c == '1':
            p = p | (2 ** i)
        else:
            raise Exception("Invalid mask value")
            
    return p


def phase2_apply_mask(mask: str, p: int):
    """Apply a bitmask to an integer"""
    mask = mask.upper()
    for i, c in enumerate(reversed(mask)):
        if c == 'X':
            # floating
            continue
        elif c == '0':
            # unchanged
            continue
        elif c == '1':
            # set bit to 1
            p = p | (2 ** i)
        else:
            raise Exception("Invalid mask value")

    return p


def phase1(data: List, mem_size: int=65536):
    
    mem = [0] * mem_size
    mask = "X" * 36
    
    for line in data:
        # parse the instruction
        op, p = line.lower().split(" = ")
        
        # do the operations
        if op == "mask":
            mask = p
        elif op.startswith("mem["):
            dp = int(op[4:-1])
            mem[dp] = apply_mask(mask, int(p))
    
    return checksum(mem)


class Mask:
    def __init__(self, mask_str: str):
        self.mask_str = mask_str.upper()
    
    def apply(self, address:int) -> Sequence[int]:
        """Apply a mask to a phase 2 address and return a sequence of all resulting addresses"""
        retval = []

        address = phase2_apply_mask(self.mask_str, address)
        mask = self.mask_str
        
        # find all the floating bits
        start = 0
        float_count = 0
        float_pos = []
        while True:
            pos = mask.find('X', start)
            if pos == -1:
                break
            
            float_count += 1
            float_pos.append(len(mask) - pos - 1)
            start = pos + 1
        float_pos.reverse()
        
        # iterate through the possible mask float values
        for float_val in range(2 ** float_count):
            for i, c in enumerate(format(float_val, 'b').zfill(float_count)):
                # set the bit to the appropriate value
                if c == '0':
                    address = address & (~ (2 ** float_pos[i]))
                elif c == '1':
                    address = address | (2 ** float_pos[i])
            retval.append(address)
        
        return retval


def phase2(data: List):
    print(f"# === Phase 2 with {len(data):d} lines")
    
    # since we have a full 36-bit address space (64 TB * sizeof(byte)), use a dict to simulate this sparse landscape  
    mem = {}
    mask = None

    for line in data:
        # parse the instruction
        op, p = line.lower().split(" = ")

        # do the operations
        if op == "mask":
            mask = Mask(p)
            print(f"mask {p}")
        elif op.startswith("mem["):
            dp = int(op[4:-1])
            for dp in mask.apply(dp):
                print(dp)
                mem[dp] = int(p)
            print(f"wrote value {p}")

    return checksum(mem.values())


def main():
    global data
    data = helper.data_list("day14.txt")

    test_data = """\
mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
mem[8] = 11
mem[7] = 101
mem[8] = 0
""".strip().split('\n')
    
    test_data_2 = """\
mask = 000000000000000000000000000000X1001X
mem[42] = 100
mask = 00000000000000000000000000000000X0XX
mem[26] = 1
""".strip().split('\n')

    print(phase1(test_data[:]))
    print(phase1(data[:]))
    print(phase2(test_data_2[:]))
    print(phase2(data[:]))

    # print("timed")
    # print(timeit.timeit('target = phase1(data)', number=100, globals=globals()))
    # print(timeit.timeit('phase1(data)', setup='target = phase1(data)', number=100, globals=globals()))
    # print(timeit.timeit('phase2(data, *target)', setup='target = phase1(data)', number=100, globals=globals()))


if __name__ == "__main__":
    main()
